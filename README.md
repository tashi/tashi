# TASHI - Test Automation Simulating Human Interaction
**TASHI** is a framework for software tests on system level (End-To-End, Blackbox or Acceptance Tests). 
It is especially designed to mimic human interactions with UI elements and asserting the response. 
With strict requirements on human readable descriptions and expectations it allows to used in official
acceptance test campaigns as well as in nightly runs. 

**TASHI** combines the knowledge and best practices from the space and medical engineering domains, 
which are both known for their strict requirements and high expectations. With **TASHI** on your side 
you can take the pressure and stay relaxed.  

For the trivia fans among you, yes, Tashi is also a Tibetan name meaning "the lucky one". 
But we don't believe in luck, when it comes to testing. However, if your tests just work, you might 
seem to be the lucky one. Tashi is here to give you just that. 

## Installation

Add this line to your application's Gemfile:

```ruby
gem 'tashi'
```

And then execute:

    $ bundle install


We follow the [Semantic Versioning](https://semver.org/) pattern.
In short semantic versioning boils down to:

- **PATCH 0.0.x** level changes for implementation level detail changes, such as small bug fixes
- **MINOR 0.x.0** level changes for any backwards compatible API changes, such as new functionality/features
- **MAJOR x.0.0** level changes for backwards incompatible API changes, such as changes that will break existing users code if they update

Thus, you should specify major and minor version in the Gemfile, e.g. `gem 'tashi', '~> X.Y'`. 

## Usage

**TASHI** is written in pure `ruby` and can be executed on plain ruby or jRuby interpreters. You can 
[learn ruby in 20 minutes](https://www.ruby-lang.org/en/documentation/quickstart/)!

Once you know just a little bit on ruby, testing with **TASHI** is really easy. Just write a simple test class like this:

```ruby 
class MyTest < Tashi::TestProcedure
  
  def initialize
    self.description = 'Test to demonstrate how easy it is to write tests.'
    self.pass_criteria = 'Procedure is passed if all tests are passed'
    self.mandatory = true
  end

  def before_procedure
    # Do your setup work here
    # Will be executed once per procedure before any test is executed.
    # You can also add checks here, to verify the system is in the state 
    # needed for the test (start criteria verification).
  end

  _instruction 'execute me'
  _expectation 'Thou shall pass'
  def some_simple_test
    Assert.equal(4, 2+2)
  end

  _instruction 'execute me'
  _expectation 'I will fail'
  _continue true
  def with_failure
    failure('soft failure') if 3 == 4
    Tashi.log.info('this will be logged!')

    failure!('hard failure') if 0 == 1
    Tashi.log.warn('this will not be logged!')
  end

  _instruction 'execute me'
  _expectation 'I have observations'
  def with_observation
    observation('first observation') unless something_expected?
    observation('second observation') if something_unexpected?
  end

end
```

To jumpstart your own project read the [Getting Started Guide](https://gitlab.gwdg.de/jhoelke/tashi/-/wikis/getting_started)
and find all the little details in our **[WIKI](https://gitlab.gwdg.de/jhoelke/tashi/-/wikis/home)**.

You should also have a look at the [Example Project](https://gitlab.gwdg.de/jhoelke/tashi-example) to see the 
framework in action. 

## Extensions
For the most use cases the core framework will not be enough. 
However, you don't need extensive SSH support if you want to test a web interface. 
Our extensions are provided as gems which allows you to pull in only the extensions your project really needs 
or provide your own shared extensions.

### Exiting Extensions
The following official extensions exist currently:

- `tashi_net`: [Networking Extension](https://gitlab.gwdg.de/jhoelke/tashi_net) providing SSH, SCP, Mail, ... support
- `tashi_web`: [Web Testing Extension](https://gitlab.gwdg.de/jhoelke/tashi_web) providing Selenium support

Have a look at the [extensions](https://gitlab.gwdg.de/jhoelke/tashi/-/wikis/extensions) page in our wiki to learn more.

### Planned Extensions
The following extensions for TASHI are planned, but not yet there. 
See [TASHI Contribution Guide](https://gitlab.gwdg.de/jhoelke/tashi/-/blob/master/CONTRIBUTING.md) to 
learn how you can help.
- `tashi_mobile`: Testing capabilities for Mobile Apps (Based on Appium/Selenium)
- `tashi_orchestrate`: Capabilities for orchestrating virtualization technologies (ESXi, docker, ...)  
- ... your idea here.

## Reporting
The `tashi_reporting` project (WIP) provides a Web-Interface based on Ruby on Rails (rails)
where you can inspect the results (including the log files) from the currently running test suite and all 
test runs previously recorded. 
It also allows you to export the known test procedures and execution results to text or spreadsheet documents, such as:
- **Test Specification** (Spec) which contains a human readable description of all test procedures (including steps) 
    for a given test suite
- **System Test Report** (STR) a report document of the test suite with overview and details
- **Requirements Trace Matrix** (RTM) if your requirement references are set this will show you which requirements 
     are covered by which tests and what result they had
- **SPR Trace Matrix** (SPRTM) same as RTM but for SPRs (Software Problem Reports, Bugs) 

## Contributing

**Bug reports and pull requests** are welcome on GWDG GitLab at https://gitlab.gwdg.de/johannes.hoelken/tashi.

- This project is intended to be a safe, welcoming space for collaboration, and contributors are expected to adhere to the [code of conduct](CODE_OF_CONDUCT.md).
- If you want to get involved read the our [contribution guide](CONTRIBUTING.md).


## Code of Conduct

Everyone interacting in the Tashi project's codebases, issue trackers, chat rooms and mailing lists is expected to follow the [code of conduct](CODE_OF_CONDUCT.md).

## License

Project is licensed under [Apache 2.0](LICENSE) License.