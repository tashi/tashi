# frozen_string_literal: true

require 'simplecov'
SimpleCov.start do
  command_name 'MiniTest'

  # Enable additional reports on branch coverage
  enable_coverage :branch

  # folders to create special groups for
  add_group 'core', '/lib/tashi/core/'
  add_group 'config', '/lib/tashi/config/'
  add_group 'mixins', '/lib/tashi/mixins/'
  add_group 'models', '/lib/tashi/models/'
  add_group 'helper', '/lib/tashi/helper/'
  add_group 'migrations', '/lib/tashi/db/migrate'
  add_group 'tasks', '/lib/tashi/tasks'

  # folders to ignore
  add_filter '/test/'
end

$LOAD_PATH.unshift File.expand_path('../lib', __dir__)
require 'tashi'
require 'tashi_testing'

require 'test_utils/test_procedure_mock_base'
require 'test_utils/test_suite_mock'
require 'test_utils/rake_mock'
require 'test_utils/db_test'

# Silence logger in tests
DepP.global.logger.level = :fatal
Thread.current[:name] = 'Test'

##
# Creates a mock (@see Minitest::Mock) with expectations
# for a TestProcedure.
# @param name: the name of the procedure
# @param result: the result of the procedure (default: PASSED)
# @param mandatory: flag to determine if the pools shall be failed if the test is failed (default: false)
# @param expect_execution: if set to false the mock will raise an exception if #execute is called (default: true)
# @return Minitest::Mock
# rubocop:disable Metrics/MethodLength
# rubocop:disable Metrics/AbcSize
def test_mock(name:, result: Tashi::Result::PASSED, mandatory: false, expect_execution: true)
  test = Minitest::Mock.new
  test.expect :execute, result if expect_execution
  test.expect :mandatory, mandatory
  test.expect :start_time, 5.minutes.ago
  test.expect :messages, "Test #{name} executed with result: #{result}."
  test.expect :working_dir=, true, [String]
  test.expect :summary, 'summary'
  test.expect :name, name
  test.expect :tests, []
  6.times do
    test.expect :result, result
    test.expect :name, name
    test.expect :description, name
    test.expect :pass_criteria, name
    test.expect :passed?, (result == Tashi::Result::PASSED)
    test.expect :failed?, (result == Tashi::Result::FAILED)
    test.expect :failed?, (result == Tashi::Result::FAILED)
    test.expect :executed?, (result != Tashi::Result::NOT_EXECUTED)
  end
  test
end
# rubocop:enable Metrics/MethodLength
# rubocop:enable Metrics/AbcSize

##
# Get a file object from a resource in the global resources folder
# @param file_name can be the name of a folder or file within /resources
# @return File
def global_resource(file_name)
  File.join(File.dirname(__FILE__), '..', 'resources', file_name)
end

##
# Switch to the temp directory for the block execution
def in_working_dir
  Dir.chdir(@working_dir) { yield }
end
