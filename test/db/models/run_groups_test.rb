# frozen_string_literal: true

require_relative '../db_test_helper'

# rubocop:disable Metrics/AbcSize
# rubocop:disable Metrics/MethodLength
class RunGroupsTest < Minitest::Test
  def setup
    @run = TestRun.create!(environment: 'env', specification: 'spec', start_time: DateTime.now)
  end

  def test_creation
    group = RunGroup.find_or_create_by!(name: 'group')

    assert_equal('group', group.name)
    assert(group.root?)
    assert(group.is_a?(RunGroup))
  end

  def test_uniqueness_on_creation
    RunGroup.find_or_create_by!(name: 'group_1')
    assert_raises(ActiveRecord::RecordInvalid) { RunGroup.create!(name: 'group_1') }
  end

  def test_uniqueness_on_edit
    RunGroup.find_or_create_by!(name: 'group_1')

    grp = RunGroup.find_or_create_by!(name: 'group_2')
    grp.update(name: 'group_1')
    assert_raises(ActiveRecord::RecordInvalid) { grp.save! }
  end

  def test_dependencies
    g1 = @run.groups.create!(name: '1')

    g11 = @run.groups.create!(name: '1.1')
    GroupRelation.create!(parent_id: g1.id, child_id: g11.id)

    g12 = @run.groups.create!(name: '1.2')
    GroupRelation.create!(parent_id: g1.id, child_id: g12.id)

    g13 = @run.groups.create!(name: '1.3')
    GroupRelation.create!(parent_id: g1.id, child_id: g13.id)

    g2 = @run.groups.create!(name: '2')
    GroupRelation.create!(child_id: g2.id, parent_id: g11.id)
    GroupRelation.create!(child_id: g2.id, parent_id: g13.id)

    assert(g1.root?)
    assert(!g11.root?)
    assert(!g2.root?)

    assert_equal([g11, g12, g13], g1.children)
    assert_equal([g2], g11.children)
    assert_equal([g2], g13.children)
    assert_equal([g11, g13], g2.parents)
  end
end
# rubocop:enable Metrics/AbcSize
# rubocop:enable Metrics/MethodLength
