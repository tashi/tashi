# frozen_string_literal: true

require_relative '../db_test_helper'

class ProcedureResultTest < Minitest::Test
  def setup
    @run = TestRun.create!(environment: 'env', specification: 'spec', start_time: DateTime.now)
    @proc = Procedure.create!(name: 'foo', description: 'bar')
    @group = RunGroup.find_or_create_by!(name: 'group')
  end

  def test_creation
    result = create_result
    assert_equal([result], @run.procedure_results)
    assert_equal([result], @run.results)
    assert_equal([result], @proc.procedure_results)
    assert_equal([result], @proc.results)
    assert_equal(result.test_run_id, @run.id)
    assert_equal(result.procedure_id, @proc.id)
  end

  def test_result_acceptance
    Tashi::Result::ALL.each { |result| create_result(result: result) }
    assert_raises { create_result(result: 'foo') }
  end

  private

  def create_result(options = {})
    default = {
      procedure: @proc,
      run_group: @group,
      result: Tashi::Result::PASSED,
      start_time: DateTime.now,
      end_time: DateTime.now
    }

    @run.procedure_results.create!(default.merge(options))
  end
end
