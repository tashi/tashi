# frozen_string_literal: true

require_relative '../db_test_helper'

class ProcedureTest < Minitest::Test
  def test_creation
    Procedure.create!(name: 'foo', description: 'bar')
  end
end
