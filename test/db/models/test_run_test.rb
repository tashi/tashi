# frozen_string_literal: true

require_relative '../db_test_helper'

class TestRunTest < Minitest::Test
  def test_creation
    TestRun.create!(
      environment: 'test_env',
      specification: 'test_spec',
      name: 'demo_run',
      start_time: DateTime.now - 5
    )

    run = TestRun.find_by!(name: 'demo_run')
    assert_equal('test_env', run.environment)
    assert_equal('test_spec', run.specification)
    assert_equal(run.start_time.strftime('%Y-%m-%d_%H-%M-%S'), run.results_dir_name)
  end
end
