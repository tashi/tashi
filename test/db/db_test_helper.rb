# frozen_string_literal: true

require 'test_helper'

require 'rake'

DepP.global.reregister(:config_source) do
  File.join(File.expand_path(__dir__), 'resources')
end

Tashi.establish_db

Rake.application.load_rakefile
Rake::Task['db:create'].invoke
Rake::Task['db:migrate'].invoke
Rake::Task['db:version'].invoke
