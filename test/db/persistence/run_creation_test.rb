# frozen_string_literal: true

require_relative '../db_test_helper'

# rubocop:disable Metrics/AbcSize
# rubocop:disable Metrics/MethodLength
class RunCreationTest < Tashi::DBTest
  def test_run_creation
    suite = TestSuiteMock.new(dependencies: { a: [:b] })
    Tashi::Executor.new(suite, sleep_secs: 0.1, name: 'PersistenceCreation').run

    run = TestRun.find_by!(name: 'PersistenceCreation')
    assert_equal('MockSuite', run.specification)
    assert_equal('dummy', run.environment)
    assert(run.start_time)
  end

  def test_pool_dependency
    suite = TestSuiteMock.new(dependencies: { a: { b: [:c] }, d: %i[c e] })
    Tashi::Executor.new(suite, sleep_secs: 0.1, name: 'pool_dependency').run

    run = TestRun.find_by!(name: 'pool_dependency')
    assert_equal(%w[a b c d e], run.groups.map(&:name).sort)
    b = run.groups.find_by!(name: 'b')
    assert_equal('a', b.parents.first.name)
    assert_equal('c', b.children.first.name)

    c = run.groups.find_by!(name: 'c')
    assert_equal(%w[b d], c.parents.map(&:name).sort)

    d = run.groups.find_by!(name: 'd')
    assert_equal(%w[c e], d.children.map(&:name).sort)
  end
end
# rubocop:enable Metrics/AbcSize
# rubocop:enable Metrics/MethodLength
