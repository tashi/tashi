# frozen_string_literal: true

require_relative '../db_test_helper'

# rubocop:disable Metrics/AbcSize
# rubocop:disable Metrics/MethodLength
class ProcedurePersistenceTest < Tashi::DBTest
  def test_multiple_tests_one_run
    run = prime_db(run_name: 'test_persistence', pool_name: 'default')
    test1 = test_mock(name: 'test1')
    test2 = test_mock(name: 'test2', result: Tashi::Result::FAILED)
    test3 = test_mock(name: 'test3', result: Tashi::Result::NOT_EXECUTED)
    pool = Tashi::TestPool.new(name: 'default', tests: [test1, test2, test3])
    Tashi::XmlExporter.stub(:export, true) do
      pool.execute
    end

    assert_equal(3, run.results.size)

    proc1 = Procedure.find_by!(name: 'test1')
    assert_equal(1, proc1.results.where(test_run: run).size)
    assert_equal(Tashi::Result::PASSED, proc1.results.where(test_run: run).first.result)
    assert(proc1.results.where(test_run: run).first.message.include?('test1 executed with result: Passed'))

    proc2 = Procedure.find_by!(name: 'test2')
    assert_equal(1, proc2.results.where(test_run: run).size)
    assert_equal(Tashi::Result::FAILED, proc2.results.where(test_run: run).first.result)
    assert(proc2.results.where(test_run: run).first.message.include?('test2 executed with result: Failed'))
  end

  def test_multiple_runs_one_test
    run1 = prime_db(run_name: 'test_persistence_1', pool_name: 'default')

    Tashi::XmlExporter.stub(:export, true) do
      Tashi::TestPool.new(name: 'default', tests: [test_mock(name: 'test1')]).execute
    end
    assert_equal(1, run1.results.size)

    procedure = Procedure.find_by!(name: 'test1')
    assert_equal(1, procedure.results.where(test_run: run1).size)
    assert_equal(Tashi::Result::PASSED, procedure.results.where(test_run: run1).first.result)
    assert(procedure.results.where(test_run: run1).first.message.include?('test1 executed with result: Passed'))

    run2 = prime_db(run_name: 'test_persistence_2', pool_name: 'default')
    Tashi::XmlExporter.stub(:export, true) do
      Tashi::TestPool.new(name: 'default', tests: [test_mock(name: 'test1', result: Tashi::Result::FAILED)]).execute
    end
    assert_equal(1, run2.results.size)

    assert_equal(1, procedure.results.where(test_run: run2).size)
    assert_equal(Tashi::Result::FAILED, procedure.results.where(test_run: run2).first.result)
    assert(procedure.results.where(test_run: run2).first.message.include?('test1 executed with result: Failed'))
  end

  private

  def prime_db(run_name:, pool_name:)
    run = TestRun.create!(
      environment: 'test_env',
      specification: 'test_spec',
      name: run_name,
      start_time: DateTime.now - 1
    )
    rg = RunGroup.find_or_create_by!(name: pool_name)
    GroupRelation.create!(parent_id: rg.id, test_run_id: run.id)
    DepP.global.reregister(:run) { run }
    run
  end
end

# rubocop:enable Metrics/AbcSize
# rubocop:enable Metrics/MethodLength
