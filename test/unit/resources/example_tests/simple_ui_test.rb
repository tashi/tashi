# frozen_string_literal: true

##
# Example Test for testing purposes
class SimpleUiTest < Tashi::TestProcedure
  def initialize
    super
    self.description = 'Example Test "SimpleUiTest"'
    self.pass_criteria = 'None.'
  end

  _instruction 'Click on the UI.'
  _expectation 'Something happened.'
  def click_here; end

  _instruction 'Click somewhere else on the UI.'
  _expectation 'Something else happened.'
  def click_there; end
end
