# frozen_string_literal: true

##
# Example Test for testing purposes
class InstallApk < Tashi::TestProcedure
  def initialize
    super
    self.description = 'Example Test "InstallApk"'
    self.pass_criteria = 'None.'
  end

  _instruction 'Fetch the latest build from the artefact server.'
  _expectation 'This is possible.'
  def fetch_build; end

  _instruction 'Deploy the latest build to the target device.'
  _expectation 'This is possible.'
  def deploy; end
end
