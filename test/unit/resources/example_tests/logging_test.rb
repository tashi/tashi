# frozen_string_literal: true

##
# Example Test for testing purposes
class LoggingTest < Tashi::TestProcedure
  def initialize
    super
    self.description = 'Example Test "LoggingTest".'
    self.pass_criteria = 'will always pass.'
    @sprs = %w[SPR-TEST-123 SPR-TEST-124]
    @requirements = %w[REQ-0815 REQ-1337]
  end

  def before_procedure
    Tashi.log.info('I was executed before the steps...')
  end

  _instruction 'Log something at warning level.'
  _expectation 'This is possible.'
  def log_at_warn
    Tashi.log.warn('This is your final warning!')
  end

  _instruction 'Log something at error level.'
  _expectation 'This is possible.'
  def log_at_error
    Tashi.log.error('Here you have the error!')
  end

  _instruction 'This should go wrong.'
  _expectation 'Something terrible will happen.'
  def fail_something
    failure!('Something terrible happened!')
    Tashi.log.error('This should not be logged')
  end

  _instruction 'This should not be executed.'
  _expectation 'nothing happened.'
  def dont_execute
    Tashi.log.error('This should not be logged, as the previous step failed')
  end

  def after_procedure
    Tashi.log.info('I was executed after the steps...')
  end
end
