# frozen_string_literal: true

##
# Example Test for testing purposes
class RefinedUiTest < Tashi::TestProcedure
  def initialize
    super
    self.description = 'Example Test "RefinedUiTest"'
    self.pass_criteria = 'None.'
  end

  _instruction 'Click on the UI.'
  _expectation 'Something happened.'
  def click_here; end

  _instruction 'Click somewhere else on the UI.'
  _expectation 'Something else happened.'
  def click_there; end
end
