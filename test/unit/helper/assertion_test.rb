# frozen_string_literal: true

require_relative '../unit_test_helper'

class AssertionTest < Tashi::UnitTest
  def test_equal
    # The following assertions should not fail
    Assert.equal('foo', 'foo')
    Assert.equal(1, 1)
    Assert.equal(:test, :test)
    Assert.equal([1, nil, false, true, 'test'], [1, true, nil, false, 'test'])
    Assert.equal(%w[a b c], %w[b c a])

    # The following assertions should go wrong
    assert_raises(TestFailure) { Assert.equal('foo', 'bar') }
    assert_raises(TestFailure) { Assert.equal(%w[a b], %w[c d]) }
    assert_raises(TestFailure) { Assert.equal(2, :two) }
    assert_raises(TestFailure) { Assert.equal([1, nil, false, true], [true, nil, false, 'test']) }
  end

  def test_file_equality
    File.open("#{@working_dir}/a.txt", 'w') { |f| f.write 'test' }
    File.open("#{@working_dir}/b.txt", 'w') { |f| f.write 'test' }
    File.open("#{@working_dir}/c.txt", 'w') { |f| f.write 'something else' }

    Assert.equal("#{@working_dir}/a.txt", "#{@working_dir}/b.txt")
    assert_raises(TestFailure) { Assert.equal("#{@working_dir}/a.txt", "#{@working_dir}/c.txt") }
  end

  def test_substring
    # The following assertions should not fail
    Assert.substring('foobarbaz', 'obarb')
    Assert.substring('foobarbaz', 'ba', occurrences: 2)

    # The following assertions should go wrong
    assert_raises(TestFailure) { Assert.substring('foo', 'bar') }
    assert_raises(TestFailure) { Assert.substring('foobarbaz', 'ba', occurrences: 1) }
    assert_raises(TestFailure) { Assert.substring('foobarbaz', 'ba', occurrences: 3) }
  end

  def test_file_contains
    File.open("#{@working_dir}/lorem.txt", 'w') { |f| f.write "Lorem ipsum\ndolor set\namet." }

    Assert.file_contains("#{@working_dir}/lorem.txt", 'dolor set')
    Assert.file_not_contains("#{@working_dir}/lorem.txt", 'foo')
    assert_raises(TestFailure) { Assert.file_contains("#{@working_dir}/lorem.txt", 'bar') }
    assert_raises(TestFailure) { Assert.file_not_contains("#{@working_dir}/lorem.txt", 'ipsum') }
    assert_raises(IllegalArgumentError) { Assert.file_contains("#{@working_dir}/lorem.txt", "set\namet") }
  end

  def test_the_truth
    Assert.truthy(true)
    Assert.truthy('')
    Assert.truthy(0)

    assert_raises(TestFailure) { Assert.truthy(nil) }
    assert_raises(TestFailure) { Assert.truthy(false) }
  end

  def test_the_falseness
    assert_raises(TestFailure) { Assert.falsy(true) }
    assert_raises(TestFailure) { Assert.falsy('') }
    assert_raises(TestFailure) { Assert.falsy(0) }

    Assert.falsy(nil)
    Assert.falsy(false)
  end

  def test_close_enough
    Assert.close_enough(0.00001, 0.00009)
    assert_raises(TestFailure) { Assert.close_enough(0.0001, 0.0003) }
  end
end
