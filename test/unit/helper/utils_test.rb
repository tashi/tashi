# frozen_string_literal: true

require_relative '../unit_test_helper'

class UtilsTest < Tashi::UnitTest
  def test_stopwatch
    # should always round up to 1, as we do not perform any action
    assert_equal(1, Utils.stopwatch(:ms) {}.first)
    assert_equal(1, Utils.stopwatch(:cs) {}.first)
    assert_equal(1, Utils.stopwatch {}.first)

    elapsed, result = Utils.stopwatch(:us) { 2**42 }
    assert(elapsed > 1)
    assert_equal(2**42, result)
  end

  def test_wait_for
    mock = Minitest::Mock.new
    mock.expect :result, nil
    mock.expect :result, false
    mock.expect :result, true

    Utils.wait_for(interval: 0.0) { mock.result }

    mock.verify
  end

  def test_wait_for_error
    assert(!Utils.wait_for(interval: 0.0, timeout: 0.01, fail_on_timeout: false) { false })
    assert_raises(TestFailure) { Utils.wait_for(timeout: 0, interval: 0.0) { false } }
  end
end
