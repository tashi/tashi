# frozen_string_literal: true

require_relative '../unit_test_helper'

class NameGeneratorTest < Tashi::UnitTest

  def test_generate
    3.times do
      assert(NameGenerator.generate_name.is_a?(String))
    end
  end

  def test_compose
    3.times do
      assert(NameGenerator.composed_name.include?('-'))
    end
  end
end
