# frozen_string_literal: true

require_relative '../unit_test_helper'

class TestFileTest < Tashi::UnitTest
  def setup
    super
    DepP.global.reregister(:testfiles_source) { File.join(File.dirname(__FILE__), '..', 'resources') }
  end

  def test_generate
    random_file = TestFile.generate(@working_dir)
    assert(File.basename(random_file).start_with?('RandomTestFile_'))
    assert_equal('.txt', File.extname(random_file))
    assert(File.exist?(random_file))
    assert(File.size(random_file) >= 4567)
    assert(File.read(random_file).include?('RandomTestFile generated '))
  end

  def test_from_template
    replacements = { name: 'foo', text: 'Lorem Ipsum Dolor' }
    test_file = TestFile.from_template(@working_dir, 'templates/example.xml', replacements)

    assert(File.basename(test_file).start_with?('example_'))
    assert_equal('.xml', File.extname(test_file))
    assert(File.exist?(test_file))

    content = File.read(test_file)
    replacements.each do |key, value|
      assert(!content.include?("${#{key}}"), "#{key} was not replaced!")
      assert(content.include?(value), "#{key} was not replaced correctly!")
    end
  end

  def test_md5sum
    assert_equal('be7c3161543d4f2d314d27479d29cdc4', TestFile.md5sum(file_resource('templates/example.xml')))
    assert_raises { TestFile.md5sum('/does/not/exist') }
    assert_raises { TestFile.md5sum(nil) }
  end
end
