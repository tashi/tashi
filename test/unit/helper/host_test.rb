# frozen_string_literal: true

require_relative '../unit_test_helper'

class HostTest < Tashi::UnitTest
  def setup
    super
    DepP.global.reregister(:known_hosts) { [] }
  end

  def test_construction
    host = Host.new(name: 'test', ip: '127.0.0.1', fqdn: 'localhost', type: :virtual)

    assert_instance_of(Host, host)
    assert_equal('localhost', host.address)
    assert_equal('127.0.0.1', host.ip.to_s)
    assert_equal(:virtual, host.type)
  end

  def test_construction_with_ip4
    host = Host.new(name: 'test', ip: '127.0.0.1')

    assert_instance_of(Host, host)
    assert_equal('127.0.0.1', host.address)
  end

  def test_construction_with_ip6
    host = Host.new(name: 'test', ip: '::1')

    assert_instance_of(Host, host)
    assert_equal('::1', host.address)
  end

  def test_construction_with_fqdn
    host = Host.new(name: 'test', fqdn: 'localhost')

    assert_instance_of(Host, host)
    assert_equal('localhost', host.address)
  end

  def test_construction_errors
    assert_raises(IllegalArgumentError) { Host.new(name: 'foo') }
    assert_raises(IPAddr::InvalidAddressError) { Host.new(name: 'foo', ip: '999.999.999.999') }
    assert_raises(IPAddr::InvalidAddressError) { Host.new(name: 'foo', ip: 'localhost') }
  end

  def test_uniqueness_of_name
    Host.new(name: 'test', ip: '::1')
    assert_raises(IllegalArgumentError) { Host.new(name: 'test', ip: '127.0.0.1') }
  end

  def test_find_host
    assert(!Host.exists?('test'), 'Host was not created yet.')

    host = Host.new(name: 'test', ip: '::1')
    assert(Host.exists?('test'), 'Host should be found now')
    assert(!Host.exists?('foo'), 'Host does not exist')
    assert_equal(host, Host.find('test'))
  end

  def test_properties
    host = Host.new(name: 'test', ip: '::1')
    host[:foo] = :bar
    host[:baz] = :qwurx

    assert_equal(:bar, host[:foo])
    assert_equal(:qwurx, host[:baz])
    assert_nil(host[:foobarbaz])

    assert_equal({ foo: :bar, baz: :qwurx }, Host.find('test').properties)
  end

  def test_ssh_properties
    Host.new(name: 'test', ip: '::1')

    assert_equal('root', Host.find('test').user)

    Host.find('test').ssh_properties = { user: 'test', password: 'test123', port: 22 }
    assert_equal({ user: 'test', password: 'test123', port: 22 }, Host.find('test').ssh_properties)
    assert_equal('test', Host.find('test').user)
  end
end
