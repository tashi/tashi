# frozen_string_literal: true

require_relative '../unit_test_helper'
class InMemoryLogTest < Tashi::UnitTest
  def setup
    super

    @logger = InMemoryLog.new
    @logger.info('Test 123')
    @logger.info('Test 456')
    @logger.warn('Test 789')
  end

  def test_counter
    assert_equal(['Test 123', 'Test 456'], @logger.info_messages)
    assert_equal(2, @logger.times_logged('Test', level: :info))
    assert_equal(0, @logger.times_logged('Test', level: :debug))
    assert_equal(1, @logger.times_logged('Test', level: :warn))
    assert_equal(0, @logger.times_logged('Test', level: :error))
    assert_equal(0, @logger.times_logged('Test', level: :fatal))
  end

  def test_recognizer
    assert(@logger.ever_logged?('Test 7'))
    assert(@logger.never_logged?('Test 9'))
  end
end
