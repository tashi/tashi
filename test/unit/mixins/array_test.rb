# frozen_string_literal: true

require_relative '../unit_test_helper'

class ArrayTest < Minitest::Test
  def test_symbolize_non_hashes
    assert_equal([], [].keys_as_symbols!)
    assert_equal([], [].keys_as_symbols)
    assert_equal(['foo', :bar, 1, [], nil, false], ['foo', :bar, 1, [], nil, false].keys_as_symbols!)
    assert_equal(['foo', :bar, 1, [], nil, true], ['foo', :bar, 1, [], nil, true].keys_as_symbols)
  end

  def test_symbolize_hashes
    array = [{ 'foo' => :bar }, { 'baz' => { 'qwurx' => 1 } }]
    expectation = [{ foo: :bar }, { baz: { 'qwurx' => 1 } }]

    assert_equal(expectation, array.keys_as_symbols)
    assert_equal([{ 'foo' => :bar }, { 'baz' => { 'qwurx' => 1 } }], array, 'original is unchanged')

    array.keys_as_symbols!
    assert_equal(expectation, array, 'Original is changed')
  end

  def test_deep_symbolize
    array = [[{ 'foo' => :bar }, { 'baz' => { 'qwurx' => 1 } }], []]
    expectation = [[{ foo: :bar }, { baz: { qwurx: 1 } }], []]

    assert_equal(expectation, array.keys_as_symbols(recursive: true))
    assert_equal([[{ 'foo' => :bar }, { 'baz' => { 'qwurx' => 1 } }], []], array, 'original is unchanged')

    array.keys_as_symbols!(recursive: true)
    assert_equal(expectation, array, 'Original is changed')
  end

  def test_hash_with_array_value
    hash = { 'foo' => [{ 'bar' => :baz }] }
    expectation = { foo: [{ bar: :baz }] }

    assert_equal(expectation, hash.keys_as_symbols(recursive: true))
    assert_equal({ 'foo' => [{ 'bar' => :baz }] }, hash, 'original is unchanged')

    hash.keys_as_symbols!(recursive: true)
    assert_equal(expectation, hash, 'Original is changed')
  end
end
