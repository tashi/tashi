# frozen_string_literal: true

require_relative '../unit_test_helper'

class HashTest < Minitest::Test
  def test_symbolize
    hash = { 'foo' => 'bar', :baz => 'qwurx', 1 => :one }
    assert_equal({ foo: 'bar', baz: 'qwurx', 1 => :one }, hash.keys_as_symbols!)
  end

  def test_symbolize_unrecursive
    hash = { 'foo' => { 'bar' => :baz } }
    hash.keys_as_symbols!
    assert_equal({ foo: { 'bar' => :baz } }, hash)
  end

  def test_symbolize_recursive
    hash = { 'foo' => { 'bar' => :baz } }
    hash.keys_as_symbols!(recursive: true)
    assert_equal({ foo: { bar: :baz } }, hash)
  end

  def test_symbolize_recursive_save
    hash = { 'foo' => { 'bar' => :baz } }
    assert_equal({ foo: { bar: :baz } }, hash.keys_as_symbols(recursive: true))
    assert_equal({ 'foo' => { 'bar' => :baz } }, hash, 'Original should be untouched')
  end
end
