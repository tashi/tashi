# frozen_string_literal: true

require_relative '../unit_test_helper'

# rubocop:disable Metrics/AbcSize
class TestSuiteTest < Tashi::UnitTest
  def setup
    super
    DepP.global.reregister(:environment_source) { file_resource('environments') }
    DepP.global.reregister(:test_source) { file_resource('example_tests') }
  end

  def test_initialization
    suite = Tashi::TestSuite.new(name: 'test', environment: 'empty_env')
    assert_equal('test', suite.name)
    assert_equal(0, suite.test_count)
  end

  def test_load_yaml
    suite = Tashi::TestSuite.from_file(file_resource('minimal_test_suit_config.yml'))

    assert_equal('minimal suite', suite.name)
    assert_equal(3, suite.test_count)
    assert(suite.groups.keys.include?('install'))
    assert(suite.groups.keys.include?('basics'))

    loaded_tests = suite.groups.values.flatten
    assert(loaded_tests.any? { |t| t.name = 'InstallApk' })
    assert(loaded_tests.any? { |t| t.name = 'SimpleUiTest' })

    assert_equal(suite, suite.load)
  end
end
# rubocop:enable Metrics/AbcSize
