# frozen_string_literal: true

require_relative '../unit_test_helper'

# rubocop:disable Metrics/AbcSize
# rubocop:disable Metrics/MethodLength
class PoolRegistryTest < Tashi::UnitTest
  def setup
    super
    @groups = {}
    @dependencies = {}

    @suite = Minitest::Mock.new
    @suite.expect :groups, @groups
    @suite.expect :dependencies, @dependencies
  end

  def test_pools_local_no_deps
    @groups[:a] = nil
    @groups[:b] = []

    registry = PoolRegistry.new(@suite)

    assert_equal(%w[a b], registry.pools.map(&:name))
    assert_empty(DepP.global.pools)
  end

  # rubocop:disable Metrics/CyclomaticComplexity
  def test_pool_dependencies
    @groups['a'] = []
    @groups['b'] = []
    @groups['c'] = []
    @groups['d'] = []
    @groups['e'] = []
    @groups['f'] = []
    @dependencies['a'] = { 'b' => ['c'], 'd' => nil }
    @dependencies['e'] = ['d']
    registry = PoolRegistry.new(@suite)

    registry.pools.each do |p|
      case p.name
      when 'b' then assert_equal(['a'], p.parents.map(&:name), "#{p.name} parents not as expected")
      when 'c' then assert_equal(['b'], p.parents.map(&:name), "#{p.name} parents not as expected")
      when 'd' then assert_equal(%w[a e], p.parents.map(&:name), "#{p.name} parents not as expected")
      else assert_empty(p.parents, "#{p.name} had parents")
      end
    end
  end
  # rubocop:enable Metrics/CyclomaticComplexity

  def test_pools_global
    @groups['a'] = nil
    @groups['b'] = []
    @dependencies['a'] = { 'b' => ['c'] }

    registry = PoolRegistry.new(@suite, globally: true)

    assert_equal(%w[a b c], registry.pools.map(&:name))
    assert_equal(%w[a b c], DepP.global.pools.map(&:name))
  end
end
# rubocop:enable Metrics/AbcSize
# rubocop:enable Metrics/MethodLength
