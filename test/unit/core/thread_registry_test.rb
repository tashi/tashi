# frozen_string_literal: true

require_relative '../unit_test_helper'

# rubocop:disable Metrics/MethodLength
class ThreadRegistryTest < Tashi::UnitTest
  def teardown
    super
    Tashi::ThreadRegistry.clear(:test_registry)
  end

  def test_call_all
    logger = InMemoryLog.new
    DepP.global.reregister(:logger) { logger }

    entry1 = Minitest::Mock.new
    entry1.expect :test, nil
    Tashi::ThreadRegistry.register(test_registry: entry1)

    entry2 = Minitest::Mock.new
    Tashi::ThreadRegistry.register(test_registry: entry2)

    Tashi::ThreadRegistry.call_all(test_registry: :test)

    entry1.verify
    entry2.verify
    assert_equal(1, logger.times_logged('does not respond to test', level: :warn))
  end

  def test_yield_all
    counter = 0
    entry = Object.new
    Tashi::ThreadRegistry.register(test_registry: entry)
    Tashi::ThreadRegistry.yield_all(:test_registry) do |block_arg|
      counter += 1
      assert_equal(entry, block_arg)
    end
    assert_equal(1, counter)
  end

  # rubocop:disable Metrics/AbcSize
  def test_threaded
    logger = InMemoryLog.new
    DepP.global.reregister(:logger) { logger }

    object1 = Minitest::Mock.new
    object1.expect :foo, nil
    object2 = Minitest::Mock.new
    object2.expect :bar, nil

    Tashi::ThreadRegistry.register(test_registry: object1)
    thread = Thread.new do
      # Work with registry in other thread
      Tashi::ThreadRegistry.register(test_registry: object2)
      Tashi::ThreadRegistry.call_all(test_registry: :bar)
      Tashi::ThreadRegistry.clear(:test_registry)
    end

    # Ensure exceptions are raised and wait for other thread
    thread.report_on_exception = true
    thread.join

    # Ensure the first mock is still in the registry
    Tashi::ThreadRegistry.call_all(test_registry: :foo)

    object1.verify
    object2.verify
    assert(logger.never_logged?('does not respond to test'))
  end
  # rubocop:enable Metrics/AbcSize
end
# rubocop:enable Metrics/MethodLength
