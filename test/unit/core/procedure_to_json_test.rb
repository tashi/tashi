# frozen_string_literal: true

require 'tempfile'
require 'json'

require_relative '../unit_test_helper'

require_relative '../resources/example_tests/logging_test'
require_relative '../resources/example_tests/install_apk'

class ProcedureToJsonTest < Tashi::UnitTest
  def setup
    super
    DepP.global.logger.level = :error
  end

  def test_export_to_json
    test = LoggingTest.new
    test.execute

    json = JSON.parse(Tashi::JsonExporter.export(test))

    assert_equal('LoggingTest', json['name'])
    assert_equal(%w[SPR-TEST-123 SPR-TEST-124], json['sprs'])
    assert_equal(%w[REQ-0815 REQ-1337], json['requirements'])
    assert_equal('Failed', json['execution']['result'])
    assert_equal(6, json['steps'].size)
  end

  def test_export_to_file
    test = InstallApk.new
    test.execute

    with_tempfile('json_export-') do |tempfile|
      Tashi::JsonExporter.export(test, tempfile.path)

      tempfile.rewind
      json = JSON.parse(tempfile.read)
      assert_equal('InstallApk', json['name'])
      assert_equal('Passed', json['execution']['result'])
      assert_equal(4, json['steps'].size)
    end
  end

  private

  def with_tempfile(basename)
    tempfile = Tempfile.new(basename)
    yield tempfile
  ensure
    tempfile.close
    tempfile.unlink
  end
end
