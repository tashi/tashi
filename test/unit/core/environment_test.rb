# frozen_string_literal: true

require_relative '../unit_test_helper'

# rubocop:disable Metrics/AbcSize
# rubocop:disable Metrics/MethodLength
class EnvironmentTest < Tashi::UnitTest
  def setup
    super
    DepP.global.reregister(:environment_source) { file_resource('environments') }
  end

  def test_initialization
    env = Tashi::Environment.new('empty_env')
    assert_equal('EMPTY', env.name)
  end

  def test_hosts_initialization
    env = Tashi::Environment.new(File.join(file_resource('environments'), 'minimal_environment.yml'))
    assert_equal('Minimal', env.name)

    hosts = DepP.global.host
    assert(hosts.exists?('localhost'))
    assert_equal('localhost', hosts.find('localhost').address)

    assert(hosts.exists?('host1'))
    assert_equal('1.2.3.4', hosts.find('host1').address)
    assert_equal(:unknown, hosts.find('host1').type)
    assert_equal({}, hosts.find('host1').properties)

    assert(hosts.exists?('host2'))
    assert_equal(:virtual, hosts.find('host2').type)
    assert_equal({ foo: :bar, baz: 'qwurx' }, hosts.find('host2').properties)
    assert_equal({ user: 'root', password: 'test123', port: 22 }, hosts.find('host2').ssh_properties)
  end
end
# rubocop:enable Metrics/AbcSize
# rubocop:enable Metrics/MethodLength
