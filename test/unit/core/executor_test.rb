# frozen_string_literal: true

require_relative '../unit_test_helper'

# rubocop:disable Metrics/MethodLength
# rubocop:disable Metrics/AbcSize
class ExecutorTest < Tashi::UnitTest
  def test_initialization
    test_a1 = test_mock(name: 'test a.1')
    test_a2 = test_mock(name: 'test a.2', result: Tashi::Result::FAILED)
    test_b1 = test_mock(name: 'test b.1')
    test_b2 = test_mock(name: 'test b.2', result: Tashi::Result::NOT_EXECUTED)
    test_c = test_mock(name: 'test c', result: Tashi::Result::FAILED)
    suite = TestSuiteMock.new(
      groups: {
        a: [test_a1, test_a2],
        b: [test_b1, test_b2],
        c: [test_c]
      }
    )
    executor = Tashi::Executor.new(suite, sleep_secs: 0)
    Tashi::XmlExporter.stub(:export, true) { executor.run }

    assert_equal(5, executor.count)
    assert_equal(2, executor.count(:passed))
    assert_equal(2, executor.count(:failed))
    assert_equal(1, executor.count(:not_exec))
  end

  def test_logging
    logger = InMemoryLog.new
    DepP.global.reregister(:logger) { logger }

    test_a1 = test_mock(name: 'test a.1')
    test_a2 = test_mock(name: 'test a.2')
    test_b1 = test_mock(name: 'test b.1', result: Tashi::Result::FAILED, mandatory: true)
    test_b2 = test_mock(name: 'test b.2', result: Tashi::Result::NOT_EXECUTED)
    test_c = test_mock(name: 'test c', result: Tashi::Result::NOT_EXECUTED)
    suite = TestSuiteMock.new(
      groups: {
        a: [test_a1, test_a2],
        b: [test_b1, test_b2],
        c: [test_c]
      },
      dependencies: { a: { b: [:c] } }
    )
    executor = Tashi::Executor.new(suite, sleep_secs: 0.5)
    Tashi::XmlExporter.stub(:export, true) { executor.run }

    info = logger.info_messages.join("\n")
    warn logger.error_messages.join("\n")
    assert(info.include?("'c' aborted: Parent pool failed!"))
    assert(info.include?("Test Run for 'MockSuite' on environment 'dummy' finished!"))
    assert(info.include?("TestPool 'a' [Passed]"))
    assert(info.include?("TestPool 'b' [Failed]"))
    assert(info.include?("TestPool 'c' [Not Executed]"))
    assert(info.include?('3 pool(s) with 2 of 5 tests passed, 1 failed and 2 not executed.'))
    assert_empty(logger.error_messages.join("\n"))
  end

  def test_unconfigured_groups
    logger = InMemoryLog.new
    DepP.global.reregister(:logger) { logger }

    test_a = test_mock(name: 'test a')
    test_c = test_mock(name: 'test c')
    suite = TestSuiteMock.new(
      groups: {
        a: [test_a],
        c: [test_c]
      },
      dependencies: { a: { b: [:c] } }
    )
    executor = Tashi::Executor.new(suite, sleep_secs: 0.5)
    Tashi::XmlExporter.stub(:export, true) { executor.run }

    info = logger.info_messages.join("\n")
    assert(info.include?('Starting pool: a'))
    assert(info.include?('Starting pool: b'))
    assert(info.include?('Starting pool: c'))
  end

  def test_double_execution
    suite = TestSuiteMock.new(groups: {}, dependencies: {})
    executor = Tashi::Executor.new(suite, sleep_secs: 0.5)

    assert_equal(:pending, executor.status)
    Tashi::XmlExporter.stub(:export, true) { executor.run }

    assert_equal(:finished, executor.status)
    assert_raises(IllegalStateError) { executor.run }
  end
end
# rubocop:enable Metrics/MethodLength
# rubocop:enable Metrics/AbcSize
