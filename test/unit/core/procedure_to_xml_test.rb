# frozen_string_literal: true

require 'tempfile'

require_relative '../unit_test_helper'

require_relative '../resources/example_tests/logging_test'
require_relative '../resources/example_tests/install_apk'

class ProcedureToXMLTest < Tashi::UnitTest
  def test_export_to_xml
    DepP.global.logger.level = :error
    test = LoggingTest.new
    test.execute

    xml = Tashi::XmlExporter.export(test)
    assert_valid_xml(global_resource('xml/tashi_procedure.xsd'), xml)
  end

  def test_export_to_file
    test = InstallApk.new
    test.execute

    tempfile = Tempfile.new('xml_export-')
    Tashi::XmlExporter.export(test, tempfile.path)

    tempfile.rewind
    assert_valid_xml(global_resource('xml/tashi_procedure.xsd'), tempfile.read)
  ensure
    tempfile.close
    tempfile.unlink
  end

  private

  def assert_valid_xml(schema_location, xml)
    xsd = Nokogiri::XML::Schema(File.read(schema_location))
    assert_empty(xsd.validate(Nokogiri::XML(xml)))
  end
end
