# frozen_string_literal: true

require_relative '../unit_test_helper'

require 'tashi/core/loader'

# rubocop:disable Metrics/MethodLength
class LoaderTest < Tashi::UnitTest
  def setup
    super
    @logger = InMemoryLog.new
    DepP.global.reregister(:logger) { @logger }
  end

  def test_help_message
    stub = proc { |arg| @logger.info(arg) }

    loader = Tashi::Loader.new('help')
    loader.stub(:puts, stub) do
      loader.run
    end

    info = @logger.info_messages.join("\n")
    assert(info.include?("Welcome to TASHI #{Tashi::VERSION}"))
  end

  def test_project_setup
    raker = RakeMock.new
    raker.expect(:invoke, :called)
    raker.expect(:load_rakefile, :called)

    require 'rake'
    Rake.stub(:application, raker) do
      assert_equal(:called, Tashi::Loader.new('setup').run)
    end

    # ensure all expectations are met
    raker.verify
  end

  def test_single_test_execution
    loader = Tashi::Loader.new('exec', 'test')

    executor = Minitest::Mock.new
    executor.expect :run, :called

    Dir.stub(:[], %w[foo/my_test.rb bar/your_test.rb non/of_your_business.rb]) do
      Tashi::Executor.stub(:new, executor) do
        assert_equal(:called, loader.run)
      end
    end

    executor.verify

    info = @logger.info_messages.join("\n")
    assert(info.include?('Target: exec/test'))
    assert(info.include?('["my_test", "your_test"]'))
  end

  def test_single_test_no_match
    raises_exception = -> { raise StandardError, 'EXIT' }

    loader = Tashi::Loader.new('exec', 'test')
    Dir.stub(:[], []) do
      loader.stub(:exit, raises_exception) do
        assert_raises { loader.run }
      end
    end

    error = @logger.error_messages.join("\n")
    assert(error.include?("No test files matching 'test' found"))
  end

  def test_suite_no_match
    raises_exception = -> { raise StandardError, 'EXIT' }

    loader = Tashi::Loader.new('run', 'suite')
    File.stub(:exist?, false) do
      loader.stub(:exit, raises_exception) do
        assert_raises { loader.run }
      end
    end

    error = @logger.error_messages.join("\n")
    assert(error.include?('The targeted test suite does not exist'))
  end

  def test_suite_match
    executor = Minitest::Mock.new
    executor.expect :run, :called

    loader = Tashi::Loader.new('run', 'suite')
    File.stub(:exist?, true) do
      Tashi::TestSuite.stub(:from_file, :called) do
        Tashi::Executor.stub(:new, executor) do
          assert_equal(:called, loader.run)
        end
      end
    end

    executor.verify

    info = @logger.info_messages.join("\n")
    assert(info.include?('suites/suite.yml'))
  end
end
# rubocop:enable Metrics/MethodLength
