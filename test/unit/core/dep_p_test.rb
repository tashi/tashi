# frozen_string_literal: true

require_relative '../unit_test_helper'

# rubocop:disable Metrics/MethodLength
# rubocop:disable Metrics/AbcSize
# rubocop:disable Metrics/ClassLength
class DepPTest < Minitest::Test
  def setup
    @depp = DepP.new
  end

  def test_initialization
    y = @depp.fork
    assert(@depp.parent.nil?, 'parent DepP should be nil')
    assert(y.parent == @depp, 'parent DepP should be x')
  end

  def test_service_registration
    @depp.register(:test) { 42 }
    assert(@depp[:test] == 42, 'DepP service should be resolved via selector')
    assert(@depp.test == 42, 'DepP service should be resolved via method')
  end

  def test_service_reregistration
    @depp.register(:test) { 42 }
    assert_equal(42, @depp.test)
    assert_raises { @depp.register(:test) { 'foo' } }

    @depp.register_or_keep(:test) { 'foo' }
    assert_equal(42, @depp.test, 'The old service should have been kept')

    @depp.reregister(:test) { 'foo' }
    assert_equal('foo', @depp.test, 'The new service should replace the old one')
  end

  def test_nil_save
    @depp.register(:nil) { nil }
    @depp.reregister(:foo) { 'foo' }
    @depp.register_or_keep(:bar) { 'bar' }
    @depp.unregister(:baz)
  end

  def test_guard_clauses
    assert_raises { @depp.register(nil) { 'foo' } }
    assert_raises { @depp.register(:bar) }
    assert_raises { @depp.register('baz') { 'qwurx' } }
    assert_raises { @depp.foobarbaz }
    assert_raises { DepP.new('foo') }
  end

  def test_service_unregistration
    @depp.register(:test) { 42 }
    assert(@depp.test, 'No exception is expected here')

    @depp.unregister(:test)
    assert_raises { @depp.test }
    assert_raises { @depp[:test] }
  end

  def test_parent_services
    sub = @depp.fork

    @depp.register(:test) { 42 }
    assert_equal(42, @depp.test)
    assert_equal(42, sub.test)

    sub.register(:test) { 'foo' }
    assert_equal(42, @depp.test)
    assert_equal('foo', sub.test)

    assert_equal(42, @depp.fork.fork.test, 'can have multiple scopes where the service is undefined')
    assert_equal('foo', sub.fork.test, 'fork is created from the current branch')
  end

  def test_parent_scope
    assert_same(@depp, @depp.fork.parent)
    assert_nil(@depp.parent)
  end

  def test_singleton_instances
    counter = 0
    @depp.register(:count) do
      # This block will be executed on demand and only once!
      counter += 1
      counter
    end

    assert_equal(0, counter, 'block should not have been executed')
    assert_equal(1, @depp.count, 'block was executed once')
    assert_equal(1, @depp.count, 'block was executed once, even if we request the service again')
    assert_equal(1, counter, 'block was executed exactly once')

    sub = @depp.fork
    assert_equal(1, sub.count, 'also call from sub scope does not create a new instance')
    assert_equal(1, counter, 'block was still executed exactly once')
  end

  def test_clear_cache
    counter = 0
    @depp.register(:count) do
      # This block will be executed on demand and only once!
      counter += 1
      counter
    end

    assert_equal(0, counter)
    assert_equal(1, @depp.count)
    @depp.clear_cache!

    assert_equal(2, @depp.count)
    assert_equal(2, counter)

    sub = @depp.fork
    assert_equal(2, sub.count)

    sub.clear_cache!(recursive: true)
    assert_equal(3, sub.count)
    assert_equal(3, counter)

    # Now we have broken the singleton pattern :shrug:
    # However, this is expected and reflected in the methods documentation
    assert_equal(4, @depp.count)
    assert_equal(3, sub.count)
    assert_equal(4, counter)
  end

  def test_global_scope
    global = DepP.global
    assert_kind_of(DepP, global)
    assert_same(global, DepP.global, 'got different instances on multiple calls')
  end

  def test_global_scope_switch
    base = DepP.global
    switched = DepP.switch_global_scope

    assert(base != switched)
    assert_kind_of(DepP, switched)
    assert_same(switched, DepP.global, 'The new scope is expected to be the global one')

    DepP.pop_global_scope
    assert_same(base, DepP.global, 'The old scope is now expected again to be the global one.')

    assert_raises { DepP.pop_global_scope }
  end
end
# rubocop:enable Metrics/MethodLength
# rubocop:enable Metrics/AbcSize
# rubocop:enable Metrics/ClassLength
