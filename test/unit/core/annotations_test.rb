# frozen_string_literal: true

require_relative '../unit_test_helper'

class AnnotationsTest < Tashi::UnitTest
  ##
  # Test class to test annotation behaviour
  class Foo
    annotate!

    _instruction 'Do this'
    _expectation 'Nothing happened'
    def my_method; end

    _instruction 'Do something else'
    _expectation 'Nothing happened'
    # Test method with comment after annotations
    def another_method; end

    # Test method with comment before annotations
    _instruction 'chill'
    _expectation 'nothing'
    def final_test; end

    # Test method without annotations
    def no_annotation; end
  end

  class Bar < Foo
    _instruction 'Derive a class'
    _expectation 'works like a charm'
    def yat_method; end
  end

  def test_single_annotation_selection
    assert_nil Foo.annotations(method: :no_annotation)
    assert_equal('Do this', Foo.annotations(method: :my_method)[:instruction])
  end

  def test_all_annotations_present
    expectation = {
      my_method: { instruction: 'Do this', expectation: 'Nothing happened' },
      another_method: { instruction: 'Do something else', expectation: 'Nothing happened' },
      final_test: { instruction: 'chill', expectation: 'nothing' }
    }
    assert_equal(expectation, Foo.annotations)
  end

  def test_annotations_in_derived_classes
    expectation = {
      my_method: { instruction: 'Do this', expectation: 'Nothing happened' },
      another_method: { instruction: 'Do something else', expectation: 'Nothing happened' },
      final_test: { instruction: 'chill', expectation: 'nothing' },
      yat_method: { instruction: 'Derive a class', expectation: 'works like a charm' }
    }
    assert_equal(expectation, Bar.annotations)
  end
end
