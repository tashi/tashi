# frozen_string_literal: true

require_relative '../unit_test_helper'

class TashiTest < Tashi::UnitTest
  def test_that_it_has_a_version
    refute_nil ::Tashi::VERSION
  end

  def test_exception_logging_full
    logger = InMemoryLog.new(level: :info)
    DepP.global.reregister(:logger) { logger }

    e = IllegalStateError.new('Unit Test Exception')
    e.set_backtrace(caller)
    Tashi.exception(e, simplify: false)

    log = logger.error_messages.join("\n")
    assert(log.include?("in `test_exception_logging_full'"))
    assert(log.include?('[IllegalStateError]: Unit Test Exception'))
    assert(log.lines.size > 7)
  end

  def test_exception_logging_simple
    logger = InMemoryLog.new(level: :info)
    DepP.global.reregister(:logger) { logger }

    e = IllegalStateError.new('Unit Test Exception')
    e.set_backtrace(caller)
    Tashi.exception(e, simplify: true)

    log = logger.error_messages.join("\n")
    assert(log.include?("in `test_exception_logging_simple'"))
    assert(log.include?('[IllegalStateError]: Unit Test Exception'))
    assert(log.lines.size < 7)
  end

  def test_exception_logging_debug
    logger = InMemoryLog.new(level: :debug)
    DepP.global.reregister(:logger) { logger }

    e = IllegalStateError.new('Unit Test Exception')
    e.set_backtrace(caller)
    Tashi.exception(e, simplify: true)

    log = logger.error_messages.join("\n")
    assert(log.include?("in `test_exception_logging_debug'"))
    assert(log.lines.size > 7)
  end

  def test_exception_logging_message
    logger = InMemoryLog.new(level: :debug)
    DepP.global.reregister(:logger) { logger }

    e = IllegalStateError.new('Unit Test Exception')
    e.set_backtrace(caller)
    Tashi.exception(e, message: 'Custom message')

    log = logger.error_messages.join("\n")
    assert(log.include?('Custom message'))
    assert(log.lines.size > 7)
  end

  def test_exception_without_trace
    logger = InMemoryLog.new(level: :info)
    DepP.global.reregister(:logger) { logger }

    e = IllegalStateError.new('Unit Test Exception')
    Tashi.exception(e, simplify: true)

    log = logger.error_messages.join("\n")
    assert(log.include?("in `test_exception_without_trace'"))
    assert(log.include?('[IllegalStateError]: Unit Test Exception'))
    assert(log.lines.size < 3)
  end
end
