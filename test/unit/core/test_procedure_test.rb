# frozen_string_literal: true

require_relative '../unit_test_helper'

class TestProcedureTest < Tashi::UnitTest
  def setup
    super
    DepP.global.reregister(:logger) { InMemoryLog.new }
  end

  class FirstTest < TestProcedureTestBase
    _instruction 'execute me'
    _expectation 'I have been executed'
    def execute_me
      count(:execute_me)
    end
  end

  def test_annotated_tests_are_executed
    test = FirstTest.new

    assert_equal(Tashi::Result::NOT_EXECUTED, test.result)
    test.execute

    assert_equal(1, test.executed[:setup])
    assert_equal(1, test.executed[:teardown])
    assert_equal(1, test.executed[:execute_me])
    assert_equal(Tashi::Result::PASSED, test.result)
  end

  class SecondTest < TestProcedureTestBase
    _instruction 'execute me'
    _expectation 'I have been executed'
    def execute_me
      count(:execute_me)
      failure('Failed as expected')
      count(:execute_me)
    end
  end

  def test_tests_can_fail
    test = SecondTest.new
    test.execute

    assert_equal(Tashi::Result::FAILED, test.result)
    assert(test.messages.inspect.include?('Failed as expected'))
    assert_equal(2, test.executed[:execute_me])
  end

  class ThirdTest < TestProcedureTestBase
    _instruction 'execute me'
    _expectation 'I have been executed'
    def execute_me
      count(:execute_me)
      failure!('Failed as expected')
      count(:execute_me) # not counted
    end
  end

  def test_tests_can_fail_hard
    test = ThirdTest.new
    test.execute

    assert_equal(Tashi::Result::FAILED, test.result)
    assert(test.messages.inspect.include?('Failed as expected'))
    assert_equal(1, test.executed[:execute_me])
  end

  class FourthTest < TestProcedureTestBase
    _instruction 'execute me'
    _expectation 'I have been executed'
    def execute_me
      observation('not quite good')
      count(:execute_me)
    end
  end

  def test_tests_can_have_observations
    test = FourthTest.new
    test.execute

    assert_equal(Tashi::Result::OBSERVATIONS, test.result)
    assert(test.messages.inspect.include?('not quite good'))
    assert_equal(1, test.executed[:execute_me])
  end

  class FifthTest < TestProcedureTestBase
    _instruction 'execute me'
    _expectation 'I have been executed'
    _continue true
    def with_failure
      count(:with_failure)
      failure('failed as expected')
      observation('not good')
    end

    _instruction 'execute me'
    _expectation 'I have been executed'
    def with_observation
      count(:with_observation)
      observation('still not so good')
    end
  end

  # rubocop:disable Metrics/AbcSize
  def test_tests_can_have_observations_and_failures
    test = FifthTest.new
    test.execute

    assert_equal(Tashi::Result::FAILED, test.result)
    assert(test.messages.inspect.include?('not good'))
    assert(test.messages.inspect.include?('still not so good'))
    assert(test.messages.inspect.include?('failed as expected'))
    assert_equal(1, test.executed[:with_observation])
    assert_equal(1, test.executed[:with_failure])
  end
  # rubocop:enable Metrics/AbcSize

  class SixthTest < TestProcedureTestBase
    _instruction 'execute me'
    _expectation 'I have been executed'
    def execute_me
      raise IllegalStateError, 'I died for an error'
    end
  end

  def test_tests_can_fail_by_exceptions
    test = SixthTest.new
    test.execute

    assert_equal(Tashi::Result::FAILED, test.result)
    assert(test.messages.inspect.include?('IllegalStateError'))
    assert(test.messages.inspect.include?('I died for an error'))
  end

  class HelperClass
    @called = false

    def self.close_hook
      @called = true
    end

    def self.called?
      @called
    end
  end

  def test_close_hook
    DepP.global.closeable_helper << HelperClass
    SixthTest.new.execute
    assert(HelperClass.called?)
  end
end
