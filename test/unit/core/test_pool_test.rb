# frozen_string_literal: true

require_relative '../unit_test_helper'
class TestPoolTest < Tashi::UnitTest
  def setup
    super
    DepP.global.reregister(:run_results) { DepP.global.results_dir }
  end

  def test_initialization
    pool = Tashi::TestPool.new(name: 'test', tests: [])

    assert(pool.root?)
    assert(pool.executable?)
    assert_nil(pool.start_time)
    assert_nil(pool.end_time)
    assert_equal(Tashi::Result::NOT_EXECUTED, pool.result)
  end

  def test_summary
    test1 = test_mock(name: 'test1')
    test2 = test_mock(name: 'test2', result: Tashi::Result::FAILED)
    test3 = test_mock(name: 'test3', result: Tashi::Result::NOT_EXECUTED)
    pool = Tashi::TestPool.new(name: 'Unit Test Pool', tests: [test1, test2, test3])

    assert_equal("TestPool 'Unit Test Pool' [Not Executed]: 1 of 3 passed, 1 failed and 1 not executed.", pool.to_s)
  end

  def test_pool_execution
    test1 = test_mock(name: 'test1')
    test2 = test_mock(name: 'test2', result: Tashi::Result::FAILED)
    pool = Tashi::TestPool.new(name: 'Unit Test Pool', tests: [test1, test2])

    Tashi::XmlExporter.stub(:export, true) do
      pool.execute
    end

    assert(!pool.executable?, 'pool should no longer be executable')
    assert(pool.start_time)
    assert(pool.end_time)
    assert_equal(Tashi::Result::PASSED, pool.result)
  end

  def test_mandatory_test_failure
    test1 = test_mock(name: 'test1', result: Tashi::Result::FAILED, mandatory: true)
    # If the second test would be executed, the mock would raise an exception
    test2 = test_mock(name: 'test2', expect_execution: false)

    pool = Tashi::TestPool.new(name: 'Unit Test Pool', tests: [test1, test2])

    Tashi::XmlExporter.stub(:export, true) do
      pool.execute
    end
    assert_equal(Tashi::Result::FAILED, pool.result)
  end

  def test_pool_execution_twice
    pool = Tashi::TestPool.new(name: 'test', tests: [])
    pool.execute
    assert_raises(IllegalStateError) { pool.execute }
  end

  def test_not_executed_parent
    root = pool_mock(result: Tashi::Result::NOT_EXECUTED)
    pool = Tashi::TestPool.new(name: 'test', tests: [], parents: [root])

    assert(!pool.root?)
    assert(!pool.executable?)
  end

  def test_failed_parent
    parent1 = pool_mock(result: Tashi::Result::FAILED)
    parent2 = pool_mock(result: Tashi::Result::PASSED)

    pool = Tashi::TestPool.new(name: 'test', tests: [])
    pool.add_parent(parent1)
    pool.add_parent(parent2)

    assert(!pool.root?)
    assert(!pool.executable?)
  end

  def test_passed_parents
    parent1 = pool_mock(result: Tashi::Result::PASSED)
    parent2 = pool_mock(result: Tashi::Result::PASSED)
    pool = Tashi::TestPool.new(name: 'test', tests: [], parents: [parent1, parent2])

    assert(!pool.root?)
    assert(pool.executable?)
  end

  def test_add_illegal_parents
    pool = Tashi::TestPool.new(name: 'test', tests: [])
    assert_raises(IllegalArgumentError) { pool.add_parent('Bar') }

    pool.execute
    assert_raises(IllegalStateError) { pool.add_parent(pool_mock(result: Tashi::Result::PASSED)) }
  end

  private

  ##
  # Creates a TestPool mock (@see Minitest::Mock) with expectations for pools.
  # @param result: the result of the pool
  # @return Minitest::Mock
  def pool_mock(result:)
    pool = Minitest::Mock.new
    pool.expect :result, result
    pool.expect :is_a?, true, [Tashi::TestPool]
  end
end
