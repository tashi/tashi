# frozen_string_literal: true

require_relative '../unit_test_helper'

require 'rake'

class DatabaseTest < Tashi::UnitTest
  def setup
    super
    Rake.application.load_rakefile
  end

  # rubocop:disable Metrics/AbcSize
  # rubocop:disable Metrics/MethodLength
  def test_generate_migration_file
    ARGV << 'foo'
    ARGV << 'test_migration'

    migrations_dir = File.join(@working_dir, 'db', 'migrate')
    FileUtils.mkdir_p(migrations_dir)

    File.stub(:expand_path, File.join(@working_dir, 'db')) do
      Rake::Task['generate:migration'].invoke
    end

    migration_file = Dir.entries(migrations_dir).find { |f| f.end_with?('test_migration.rb') }
    assert(migration_file, 'migration file not found in migrations directory.')
    content = File.read(File.join(migrations_dir, migration_file))
    assert(content.include?('class TestMigration < ActiveRecord::Migration'))
    assert(content.include?('def change'))
  end
  # rubocop:enable Metrics/AbcSize
  # rubocop:enable Metrics/MethodLength
end
