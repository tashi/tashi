# frozen_string_literal: true

require_relative '../unit_test_helper'

require 'rake'

class SetupTest < Tashi::UnitTest
  def setup
    super
    Rake.application.load_rakefile
    DepP.global.reregister(:results_dir) { File.join(Dir.pwd, 'results') }
    DepP.global.reregister(:config_source) { File.join(Dir.pwd, 'config') }
  end

  def test_construction
    in_working_dir { Rake::Task['tashi:create_project'].invoke }

    assert_sorted(
      %w[. .. .gitignore Rakefile config environments lib procedures results suites testfiles],
      Dir.entries(@working_dir)
    )
    assert_sorted(%w[. .. database.yml], Dir.entries("#{@working_dir}/config"))
    assert_sorted(%w[. .. example.yml], Dir.entries("#{@working_dir}/suites"))
    assert_sorted(%w[. .. development.yml], Dir.entries("#{@working_dir}/environments"))
    assert_sorted(%w[. .. example_test.rb], Dir.entries("#{@working_dir}/procedures"))
    assert_sorted(%w[. .. tashi.rb], Dir.entries("#{@working_dir}/lib/tashi"))
  end

  private

  def assert_sorted(exp, act)
    assert_equal(exp.sort, act.sort)
  end
end
