# frozen_string_literal: true

require 'test_helper'

##
# Get a file object from a resource in the resources folder
# @param file_name can be the name of a folder or file within test/resources
# @return File
def file_resource(file_name)
  File.join(File.dirname(__FILE__), 'resources', file_name)
end
