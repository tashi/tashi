# frozen_string_literal: true

module Tashi
  class DBTest < UnitTest
    def setup
      super
      DepP.global.reregister(:use_db?) { true }
    end
  end
end
