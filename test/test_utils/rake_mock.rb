# frozen_string_literal: true

##
# Mocking class to allow easy stubbing and mocking of Rake calls.
#
# Usage with +Object#stub+ is easy, e.g.
#
#     raker = RakeMock.new
#     raker.expect(:invoke, :called)
#     raker.expect(:load_rakefile, :called)
#
#     require 'rake'
#     Rake.stub(:application, raker) do
#       assert_equal(:called, Tashi::Loader.new('setup').run)
#     end
#
# Author johannes.hoelken@hawk.de
#
class RakeMock < Minitest::Mock
  # @return self
  def application
    self
  end

  # @return self
  def [](_arg)
    self
  end
end
