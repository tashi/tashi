# frozen_string_literal: true

class TestSuiteMock < Tashi::TestSuite
  def initialize(groups: {}, dependencies: nil, environment: 'dummy')
    @config = {}
    @groups = groups
    @dependencies = dependencies
    @environment = Minitest::Mock.new
    3.times { @environment.expect :name, environment }
  end

  def name
    'MockSuite'
  end

  def load
    self
  end
end
