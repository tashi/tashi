# frozen_string_literal: true

##
# Basic +TestProcedure+ derived class that provides simple means for testing
class TestProcedureTestBase < Tashi::TestProcedure
  def initialize(description: 'Test', pass_criteria: 'None')
    super
    @executed = {}
  end

  attr_reader :executed

  def before_procedure
    count(:setup)
  end

  def after_procedure
    count(:teardown)
  end

  protected

  def count(name)
    executed[name] ||= 0
    executed[name] += 1
  end
end
