# frozen_string_literal: true

require 'yaml'
require 'active_record'
require_relative '../../tashi'

# rubocop:disable Metrics/BlockLength
namespace :db do
  require 'erb'

  task :environment do
    DATABASE_ENV = Tashi::DATABASE_ENV
    ActiveRecord::Migrator.migrations_paths << File.join(File.expand_path(__dir__), '..', 'db', 'migrate')
  end

  task configuration: :environment do
    config_file = File.join(DepP.global.config_source, 'database.yml')
    raise "Database config not found at #{config_file}." unless File.exist?(config_file)
    config = ERB.new(File.read(config_file)).result
    @config = YAML.safe_load(config, permitted_classes: [Symbol])[Tashi::DATABASE_ENV]
  end

  task configure_connection: :configuration do
    ActiveRecord::Base.establish_connection @config
  end

  desc 'Create the database from config/database.yml for the Tashi::DATABASE_ENV'
  task create: :configure_connection do
    create_database @config
  end

  desc 'Drops the database for the Tashi::DATABASE_ENV'
  task drop: :configure_connection do
    if @config['adapter'].include?('sqlite')
      File.delete(@config['database']) if File.exist?(@config['database'])
    else
      ActiveRecord::Base.connection.drop_database @config['database']
    end
  end

  desc 'Migrate the database'
  task migrate: :configure_connection do
    ActiveRecord::Migration.verbose = true
    migrator.migrate(ENV['VERSION'] ? ENV['VERSION'].to_i : nil)
  end

  desc 'Rolls the schema back to the previous version (specify steps w/ STEP=n).'
  task rollback: :configure_connection do
    migrator.rollback(ENV['STEP'] ? ENV['STEP'].to_i : 1)
  end

  desc 'Retrieves the current schema version number'
  task version: :configure_connection do
    puts "Current version: #{ActiveRecord::Migrator.current_version}"
  end

  private

  def create_database(config, options = {})
    unless config['adapter'].include?('sqlite')
      ActiveRecord::Base.establish_connection config.merge('database' => nil)
      ActiveRecord::Base.connection.create_database config['database'], options
    end
    ActiveRecord::Base.establish_connection config
  end

  def migrator
    migrations_paths = ActiveRecord::Migrator.migrations_paths
    schema_migration = ActiveRecord::Base.connection.schema_migration
    ActiveRecord::MigrationContext.new(migrations_paths, schema_migration)
  end
end

namespace :generate do
  desc 'Generate a new migration, usage: "rake generate:migration foo_bar" or "rake generate:migration FooBar".'
  task :migration do
    avoid_argv_error
    name = name_from_argv
    generate(gen_migration_filename(name), gen_migration_content(name))
  end

  private

  # Avoid "Don't know how to build task 'foo'" if using ARGV arguments for tasks
  # rubocop:disable Style/BlockDelimiters
  def avoid_argv_error
    ARGV.each { |a| task a.to_sym do; end }
  end
  # rubocop:enable Style/BlockDelimiters

  def name_from_argv
    ARGV[1]&.gsub(' ', '_')&.underscore
  end

  def gen_migration_filename(name)
    File.join(File.expand_path(__dir__), '..', 'db', 'migrate', "#{DateTime.now.strftime('%Y%m%d%H%M%S')}_#{name}.rb")
  end

  def gen_migration_content(name)
    ar_version = ActiveRecord::VERSION::STRING.split('.')[0, 2].join('.')
    content = "##\n"
    content += "# Migration #{name}\n"
    content += "class #{name.camelize} < ActiveRecord::Migration[#{ar_version}]\n"
    content += "  # migration method\n"
    content += "  def change\n"
    content += "    # your code here\n"
    content += "  end\n"
    content += "end\n"
    content
  end

  def generate(file, content)
    File.open(file, 'w') { |f| f.puts content }
  end
end
# rubocop:enable Metrics/BlockLength
