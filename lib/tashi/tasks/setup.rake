# frozen_string_literal: true

require 'fileutils'

require_relative '../../tashi'

# rubocop:disable Metrics/BlockLength
# rubocop:disable Metrics/MethodLength
namespace :tashi do
  desc 'Create the folder structure and default files for a new tashi project'
  task :create_project do
    raise 'Project already created! Aborting.' if already_created?

    create_folder_structure
    create_sample_environment
    create_suite
    create_config
    create_example_test
    tweak_gitignore
    create_rakefile
    create_extension
    print_instructions
  end

  private

  def already_created?
    default_dirs.any? { |dir| File.exist?(dir.to_s) }
  end

  def create_folder_structure
    default_dirs.each { |dir| FileUtils.mkdir_p dir.to_s }
  end

  def create_sample_environment
    yaml = <<~YAML
      # Example environment for Tashi. Add all the hosts you need here, see documentation for options
      name: 'development'
      hosts:
        -
          name: 'localhost'
          fqdn: 'localhost'
          ip: 127.0.0.1
          properties:
            foo: :bar
          ssh_properties:
            user: 'root'
            password: 'test123'
    YAML
    File.open(File.join(DepP.global.environment_source, 'development.yml'), 'w') { |f| f.puts yaml }
  end

  def create_suite
    yaml = <<~YAML
      # Example Test Suite for Tashi. Add all tests to be executed here, see documentation for options
      name: 'example_suite'
      use_database: false
      environment: 'development'
      groups:
        -
          name: 'example'
          tests:
            - example_test
    YAML
    File.open(File.join(DepP.global.suite_source, 'example.yml'), 'w') { |f| f.puts yaml }
  end

  def create_config
    yaml = <<~YAML
      # Example database config for an SQLite3 connection. See active_record documentation for options
      :tashi:
        pool: 5
        timeout: 5000
        database: "tashi.sqlite"
        adapter: "sqlite3"
    YAML
    File.open(File.join(DepP.global.config_source, 'database.yml'), 'w') { |f| f.puts yaml }
  end

  def create_example_test
    test = <<~RUBY
      # Example test
      class ExampleTest < Tashi::TestProcedure
        def initialize
          super
          self.description = 'Example Test for Tashi.'
          self.pass_criteria = 'will never pass, if terrible things happen.'
        end
      
        def before_procedure
          Tashi.log.info('I was executed before the steps...')
        end
      
        _instruction 'Log something at warning level.'
        _expectation 'This is possible.'
        def log_at_warn
          Tashi.log.warn('This is your final warning!')
        end
      
        _instruction 'This should go wrong.'
        _expectation 'Something terrible will happen.'
        def fail_something
          failure!('Something terrible happened!')
          Tashi.log.error('This should not be logged')
        end
      
        def after_procedure
          Tashi.log.info('I was executed after the steps...')
        end
      end
    RUBY
    File.open(File.join(DepP.global.test_source, 'example_test.rb'), 'w') { |f| f.puts test }
  end

  def create_extension
    extension = <<~RUBY
      # Tashi module extension.
      # Require all gem extensions or load your custom helpers here.
      # Feel free to overwrite parts of the Framework, too.
      module Tashi
        Tashi.log.info('Custom extension loaded...')
      end
    RUBY
    extension_folder = File.join(Dir.pwd, 'lib', 'tashi')
    FileUtils.mkdir_p extension_folder.to_s
    File.open(File.join(extension_folder, 'tashi.rb'), 'w') { |f| f.puts extension }
  end

  def tweak_gitignore
    File.open('.gitignore', 'a') do |f|
      f.puts("\n/results/")
    end
  end

  def create_rakefile
    rakefile = <<~RUBY
      # Import all rake tasks provided by TASHI gem, e.g. database migration
      require 'tashi'
      spec = Gem::Specification.find_by_name 'tashi'
      Rake.add_rakelib "\#{spec.gem_dir}/lib/tashi/tasks"
    RUBY
    File.open('Rakefile', 'w') { |f| f.puts(rakefile) }
  end

  def default_dirs
    [
      DepP.global.test_source,
      DepP.global.environment_source,
      DepP.global.config_source,
      DepP.global.results_dir,
      DepP.global.testfiles_source,
      DepP.global.suite_source
    ]
  end

  # rubocop:disable Metrics/AbcSize
  def print_instructions
    Tashi.log.info('Folder structure for tashi-project was created. You will find demo content in each of them')
    Tashi.log.info('You can run the tests with')
    Tashi.log.info("\t bundle exec tashi run suite_name")
    Tashi.log.info("\t bundle exec tashi exec single_test_name")
    Tashi.log.info('If you want to use a database add the connector gem to your gemfile, edit the database.yml and run')
    Tashi.log.info("\t bundle exec rake db:create")
    Tashi.log.info("\t bundle exec rake db:migrate")
    Tashi.log.info('after that set "use_database: true" in the test suite.')
    Tashi.log.info('Try to run our example test with "tashi exec example".')
    Tashi.log.info('Happy Testing!')
  end
  # rubocop:enable Metrics/AbcSize
end
# rubocop:enable Metrics/MethodLength
# rubocop:enable Metrics/BlockLength
