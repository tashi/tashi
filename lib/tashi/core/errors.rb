# frozen_string_literal: true

##
# Error to mark tests as failed
class TestFailure < StandardError; end

##
# Error to tell that we are in an unexpected state
class IllegalStateError < StandardError; end

##
# Error to tell that we cannot work with the given argument
class IllegalArgumentError < StandardError; end
