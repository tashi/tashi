# frozen_string_literal: true

module Tashi
  ##
  # Exporter to generate +XML+ representation of test procedures.
  #
  # Author johannes.hoelken@hawk.de
  #
  class XmlExporter
    require 'nokogiri'

    attr_reader :proc

    ##
    # Class method shortcut to export a +Tashi::Procedure+ to XML
    # If the target is left blank the method will return the XML content as string
    # @param procedure the procedure to export
    # @param target file path of the export target
    def self.export(procedure, target = nil)
      return Tashi::XmlExporter.new(procedure).to_string if target.blank?

      Tashi::XmlExporter.new(procedure).to_file(target)
    end

    ##
    # Constructor
    # @param procedure the procedure to export
    def initialize(procedure)
      @proc = procedure
    end

    ##
    # Export the results to an XML string
    def to_string
      builder = Nokogiri::XML::Builder.new do |xml|
        xml.procedure(name: @proc.name) do
          basic_info(xml)
          execution_summary(xml)
          steps(xml)
        end
      end
      builder.to_xml
    end

    def to_file(target)
      File.open(target, 'w') { |file| file << to_string }
    end

    private

    def basic_info(xml)
      description(xml)
      requirements(xml)
      sprs(xml)
    end

    def description(xml)
      xml.description { xml.cdata(@proc.description) }
      xml.pass_criteria { xml.cdata(@proc.pass_criteria) }
    end

    def requirements(xml)
      xml.requirements { @proc.requirements.each { |r| xml.requirement(reference: r) } } if @proc.requirements.present?
    end

    def sprs(xml)
      xml.sprs { @proc.sprs.each { |r| xml.spr(reference: r) } } if @proc.sprs.present?
    end

    def execution_summary(xml)
      xml.execution(result: @proc.result) do
        time_info(xml)
        step_summary(xml)
        summary(xml, type: :observation)
        summary(xml, type: :failure)
      end
    end

    def time_info(xml)
      xml.time(
        start_time: @proc.start_time&.iso8601,
        end_time: @proc.end_time&.iso8601,
        duration: duration(@proc.start_time, @proc.end_time)
      )
    end

    def summary(xml, type:)
      data = tests_by_status(type)
      return if !defined?(data) || data.blank?

      xml.public_send("#{type}s".to_sym) do
        data.each do |entry|
          xml.public_send(type, step: entry.name) { xml.cdata(entry.public_send("#{type}s".to_sym)) }
        end
      end
    end

    def tests_by_status(status)
      return @proc.tests.reject { |e| e.observations.blank? } if status == :observation
      return @proc.tests.reject { |e| e.failures.blank? } if status == :failure

      nil
    end

    def step_summary(xml)
      xml.step_summary { xml.cdata(@proc.summary) }
    end

    def steps(xml)
      xml.steps { @proc.tests.each { |t| step_details(xml, t) } }
    end

    # rubocop:disable Metrics/AbcSize
    def step_details(xml, step)
      xml.step(name: step.name, result: step.result, duration: duration(step.start, step.stop)) do
        xml.instruction { xml.cdata(step.instruction) }
        xml.expectation { xml.cdata(step.expectation) }
        xml.observation { xml.cdata(step.summary) }
        xml.log { xml.cdata(step.log) } if step.log.present?
      end
    end
    # rubocop:enable Metrics/AbcSize

    def duration(start, stop)
      duration = ''
      return duration if start.blank? || stop.blank?

      ActiveSupport::Duration.build((stop - start).abs.to_i).parts.each do |k, v|
        duration += "#{v} #{k} "
      end
      duration.strip
    end
  end
end
