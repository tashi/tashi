# frozen_string_literal: true

# Namespace
module Tashi
  ##
  # The test suite is defined by a yaml file
  # it holds the information on the tests to execute,
  # the execution order and the environment.
  class TestSuite
    require 'yaml_extend'

    ##
    # Initialize the TestSuite from a YAML file
    def self.from_file(path)
      TestSuite.new(YAML.ext_load_file(path)).load
    end

    attr_reader :config, :dependencies, :groups, :environment

    ##
    # Create a Test Suite from a configuration hash
    def initialize(config)
      @config = config.keys_as_symbols(recursive: true)
      @loaded = false
      @groups = {}
    end

    ##
    # load the suite according to the configuration
    # @return the loaded suite.
    def load
      return self if @loaded

      load_tests
      load_dependencies
      load_environment
      establish_db_connection
      @loaded = true
      self
    end

    # The name of the TestSuite
    def name
      config[:name]
    end

    # count the tests within the suite
    def test_count
      result = 0
      groups.values.each { |v| result += v.size }
      result
    end

    private

    def load_tests
      Tashi.log.info("Loading TestProcedures for test specification '#{name}'")
      config[:groups]&.each(&method(:load_group))
      Tashi.log.info("### Loading complete: Found #{test_count} procedure(s) to execute.")
    end

    def load_group(group)
      Tashi.log.info("### Group: #{group[:name]} ###")
      @groups[group[:name]] =
        group[:tests]&.each_with_object([]) do |test_name, tests|
          tests << load_test(available_tests.find { |e| e.end_with?("#{test_name}.rb") })
        end
    end

    def load_test(file)
      File.foreach(file) do |line|
        next unless line.include?('class ')

        class_name = extract_class_name(line)
        next if class_name.nil?

        return load_test_class(class_name, file)
      end
      raise IllegalArgumentError, "File #{file} does not define a test class"
    end

    def load_test_class(class_name, file)
      require_relative file
      test = Object.const_get(class_name).new
      raise "#{file}: '#{class_name}' is not a Tashi::TestProcedure" unless test.is_a?(Tashi::TestProcedure)

      Tashi.log.info("Loaded [#{class_name}] from #{file}")
      test
    end

    def extract_class_name(line)
      line.match(/class (?<class_name>\S*)/)[:class_name]
    end

    def available_tests
      return @available_tests if defined?(@available_tests) && !@available_tests.nil?
      raise "Test source directory '#{DepP.global.test_source}' not found" unless Dir.exist?(DepP.global.test_source)

      @available_tests = Dir["#{DepP.global.test_source}/**/*.rb"]
    end

    def load_dependencies
      set_dependencies
      verify_dependencies
    end

    def set_dependencies
      return if config[:dependencies].nil? || config[:dependencies].empty?

      @dependencies = config[:dependencies]
    end

    def verify_dependencies
      return if dependencies.nil?

      dep_string = dependencies.inspect
      unused = groups.keys.reject { |g| dep_string.include?(g) }
      raise "The following groups are not part of the dependency hierarchy: #{unused.inspect}" unless unused.empty?
    end

    def load_environment
      @environment = Tashi::Environment.new(config[:environment])
    end

    def establish_db_connection
      Tashi.log.info("Use Database: #{config[:use_database]}")
      return unless config[:use_database]

      Tashi.establish_db
    end
  end
end
