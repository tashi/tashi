# frozen_string_literal: true

##
# Registry class to handle pools and dependencies.
# Can register pools globally with +DepP+ or just local (recommended, default)
#
# Author: johannes.hoelken@hawk.de
#
class PoolRegistry
  attr_reader :pools

  ##
  # Constructor
  # @param suite the test-suite to register pools from
  # @param sleep_secs: the time_info to sleep for each pool before checking if ready to run
  # @param globally: flag to determine if registration should be global. Default: false
  def initialize(suite, sleep_secs: 1, globally: false)
    DepP.global.register_or_keep(:pools) { [] }

    @sleep_secs = sleep_secs
    @globally = globally
    @suite = suite
    @pools = []

    register_pools
  end

  ##
  # Find pools in the registry by name
  # @param name the name of the pool to find
  # @param globally: flag to determine if the pool shall be searched globally.
  def find_pool(name, globally: @globally)
    pool = pools.find { |p| p.name == name.to_s }
    pool = DepP.global.pools.find { |p| p.name == name.to_s } if pool.nil? && globally
    pool = add_pool(name, [], globally: @globally) if pool.nil?
    pool
  end

  private

  def register_pools
    discover_pools
    build_dependencies
    persist_pools
  end

  def discover_pools
    @suite.groups.each { |name, tests| add_pool(name, tests) }
  end

  def add_pool(name, tests, globally: @globally)
    pool = Tashi::TestPool.new(name: name.to_s, tests: tests, sleep_secs: @sleep_secs)
    DepP.global.pools << pool if globally
    pools << pool
    pool
  end

  def build_dependencies(stage = @suite.dependencies)
    return unless stage.is_a?(Hash)

    stage.each do |key, children|
      if children.is_a?(Hash)
        build_dependencies(children)
        children = children.keys
      end

      parent = find_pool(key)
      children&.each { |child_name| apply_relation(parent, child_name) }
    end
  end

  def apply_relation(parent, child_name)
    child = find_pool(child_name)
    child.add_parent(parent)
    parent.add_child(child)
  end

  def persist_pools
    return unless DepP.global.use_db?

    create_db_representation
    persist_dependencies
  end

  def create_db_representation
    pools.each { |p| RunGroup.find_or_create_by!(name: p.name) }
  end

  def persist_dependencies
    pools.each do |g|
      persist_dependent(g)
      persist_independent(g)
    end
  end

  def persist_dependent(pool)
    return if pool.parents.blank?

    pool.parents.each do |p|
      GroupRelation.create!(
        child_id: RunGroup.select('id').find_by!(name: pool.name).id,
        parent_id: RunGroup.select('id').find_by!(name: p.name).id,
        test_run_id: DepP.global.run.id
      )
    end
  end

  def persist_independent(pool)
    return if pool.children.present?

    GroupRelation.create!(
      parent_id: RunGroup.select('id').find_by!(name: pool.name).id,
      test_run_id: DepP.global.run.id
    )
  end
end
