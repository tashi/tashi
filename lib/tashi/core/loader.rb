# frozen_string_literal: true

require 'rake'

module Tashi
  ##
  # Entry Point for a TASHI execution.
  #
  # ### Usage
  # Specify the mode (run, exec, setup, help) and optionally a target, e.g.
  #      Tashi::Loader.new('exec', 'my_test').run
  # you can also specify an arry with two elements, e.g.
  #      Tashi::Loader.new(ARGV).run
  #
  # Author johannes.hoelken@hawk.de
  #
  class Loader
    ##
    # Constructor
    # @param args the (ARGV) arguments for the loader
    def initialize(*args)
      args.flatten!
      @mode = args.first
      @target = args.last
    end

    ##
    # Execute +TASHI+
    def run
      Thread.current[:name] = 'main'

      case @mode
      when 'run' then test_suite
      when 'exec' then single_test
      when 'setup' then setup_project
      else print_usage
      end
    end

    private

    def test_suite
      greet
      detect_custom_extensions
      load_suite_from_file
      execute
    end

    def single_test
      greet
      detect_custom_extensions
      generate_suite
      execute
    end

    def greet
      Tashi.log.info('########### Starting TASHI ###########')
      Tashi.log.info("Core Version: #{Tashi::VERSION}")
      Tashi.log.info("Target: #{@mode}/#{@target}")
    end

    def detect_custom_extensions
      extension = File.join(Dir.pwd, 'lib', 'tashi', 'tashi.rb')
      return unless File.file?(extension)

      Tashi.log.info("Found extension module: #{extension}")
      require extension
    end

    def generate_suite
      tests = find_procedure_candidates
      Tashi.log.info("Test procedures found: #{tests.inspect}")
      @suite = Tashi::TestSuite.new(
        name: 'Development',
        environment: 'development',
        use_database: false,
        groups: [{ name: 'default', tests: tests }]
      )
    end

    def find_procedure_candidates
      candidates = Dir["#{DepP.global.test_source}/**/*.rb"].map { |c| c.split('/').last.chomp('.rb') }
      candidates.select! { |f| f.include?(@target) }
      if candidates.blank?
        Tashi.log.error("No test files matching '#{@target}' found in #{DepP.global.test_source}")
        exit(12)
      end

      candidates
    end

    def load_suite_from_file
      suite = File.join(DepP.global.suite_source, "#{@target}.yml")
      unless File.exist?(suite)
        Tashi.log.error("The targeted test suite does not exist: #{suite}")
        exit(13)
      end

      Tashi.log.info("Loading test suite: #{suite}")
      @suite = Tashi::TestSuite.from_file(suite)
    end

    def execute
      Tashi::Executor.new(@suite).run
    end

    def setup_project
      Tashi.log.info("\n########### Welcome to TASHI #{Tashi::VERSION} ###########")
      Tashi.log.info('Setting up project....')
      Rake.application.load_rakefile
      Rake::Task['tashi:create_project'].invoke
    end

    def print_usage
      puts("########### Welcome to TASHI #{Tashi::VERSION} ###########")
      puts('TASHI (Test Automation Simulating Human Interaction) is a framework for test automation on system level.')
      puts('It is especially designed to mimic human interactions with UI elements and asserting the response.')
      puts("\n### Usage")
      puts(" - Setup Project:\t tashi setup \t\t # Will create the project structure needed")
      puts(" - Run single test:\t tashi exec test_name \t # Execute all tests files matching the given pattern")
      puts(" - Run test suite:\t tashi run suite_name \t # Execute the test suite with this exact name")
      puts("\n### Help\nFurther help is available in the documentation online.\n\n")
    end
  end
end
