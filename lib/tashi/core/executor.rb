# frozen_string_literal: true

# Namespace
module Tashi
  ##
  # Test Executor will take a test suite and execute all tests in groups as they are configured
  # The Executor will schedule tests in threads, however to archive real multi-threading tests
  # must be executed in and interpreter that supports real threading as +jRuby+ or +TruffleRuby+.
  #
  # The Executor will schedule all pools simultaneously, once a pool is +#executable?+ the
  # corresponding tests will be executed in the order they appear in the +test_suite.yml+
  # configuration file.
  #
  # Author johannes.hoelken@hawk.de
  # rubocop:disable Metrics/ClassLength
  class Executor
    attr_reader :suite, :start_time, :end_time

    ##
    # Constructor
    # @param test_suite the test suite
    def initialize(test_suite, sleep_secs: 3, name: NameGenerator.composed_name)
      @suite = test_suite.load
      @sleep_secs = sleep_secs
      @name = name

      @threads = []
      @status = :pending
      @lock = Monitor.new
    end

    ##
    # Run all tests
    def run
      announce_started
      register_pools
      kick_execution
      wait_for_finished
    ensure
      summarize
    end

    ##
    # Test counter
    # @param state the state of the tests to count. Supported values
    #    - :passed wil count only passed tests
    #    - :failed wil count only failed tests
    #    - :not_exec wil count only tests not executed
    #    - :all wil count all tests (default)
    # @return the number tests within the pool
    def count(state = :all)
      counter = 0
      registry&.pools&.each { |p| counter += p.count(state) }
      counter
    end

    ##
    # Get the current pool status
    def status
      synchronized { return @status }
    end

    ##
    # Get a string summary of the current execution
    def summary
      summary = "Test Run for '#{suite.name}' on environment '#{suite.environment.name}' #{@status}!\n"
      summary += pool_summary
      summary += "Run has #{registry&.pools&.size} pool(s) with"
      summary += " #{count(:passed)} of #{count} tests passed,"
      summary += " #{count(:failed)} failed"
      summary += " and #{count(:not_exec)} not executed.\n"
      summary
    end

    private

    attr_reader :registry

    def pool_summary
      summary = ''
      registry&.pools&.sort_by(&:name)&.each { |p| summary += "\t #{p}\n" }
      summary
    end

    def announce_started
      synchronized do
        raise IllegalStateError, "Executor was already started (state: '#{@status}')!" unless @status == :pending

        Tashi.log.info("########### Start Test-Run #{@name} ###########")
        @start_time = DateTime.now
        @status = :started
        create_run_dir
        create_model
      end
    end

    def register_pools
      @registry = PoolRegistry.new(suite, sleep_secs: @sleep_secs)
    end

    def kick_execution
      synchronized { @status = :running }
      registry.pools.each do |pool|
        @threads << Thread.new do
          Thread.current[:name] = pool.name
          start_pool(pool)
        end
      end
    end

    def start_pool(pool)
      pool.wait_for_execution
      pool.execute
    rescue TestFailure => e
      Tashi.log.info("'#{pool.name}' aborted: #{e.message}")
    rescue StandardError => e
      Tashi.exception(e, message: "Unexpected error in '#{pool.name}'")
    end

    def wait_for_finished
      @threads.each(&:join)
      announce_finished
    end

    def summarize
      Tashi.log.info(summary)
    end

    # rubocop:disable Metrics/MethodLength
    def create_model
      return unless DepP.global.use_db?

      DepP.global.reregister(:run) do
        TestRun.create!(
          name: @name,
          environment: @suite.environment.name,
          specification: @suite.name,
          start_time: @start_time,
          results_dir: File.absolute_path(DepP.global.run_results)
        )
      end
      DepP.global.run
    end
    # rubocop:enable Metrics/MethodLength

    def create_run_dir
      DepP.global.reregister(:run_results) do
        require 'fileutils'
        dir = File.join(DepP.global.results_dir, @start_time.strftime('%Y-%m-%d_%H-%M-%S'))
        FileUtils.mkdir_p dir.to_s
        dir
      end
    end

    def announce_finished
      synchronized do
        @end_time = DateTime.now
        @status = :finished
        DepP.global.run.update!(end_time: @end_time) if DepP.global.use_db?
      end
    end

    ##
    # Executes a given block in an instance-synchronization context
    def synchronized
      @lock.synchronize { yield }
    end
  end
  # rubocop:enable Metrics/ClassLength
end
