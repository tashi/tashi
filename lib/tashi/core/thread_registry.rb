# frozen_string_literal: true

module Tashi
  # The ThreadRegistry is a registration place to store information and object references
  # by keyword in the scope of the *current* Thread.
  #
  # Common use case is the TASHI auto-close mechanism / the close_hook.
  # Within a class it could be implemented as follows:
  #
  #   def self.close_hook
  #     ThreadRegistry.call_all(foo_sessions: :close)
  #     ThreadRegistry.clear(:foo_sessions)
  #   end
  #
  #   def initialize
  #     # do initialisation
  #     # register self
  #     ThreadRegistry.register(foo_sessions: self)
  #   end
  #
  #   def close
  #     # cleanup
  #   end
  #
  # However, the use-case is not limited to that, you can also store and pass metadata
  # within a thread, etc. Registry is implemented as a singleton and exposes all public
  # methods also as class methods for convenience.
  #
  # Author johannes.hoelken@hawk.de
  #
  class ThreadRegistry
    include Singleton

    # Call a method on all entries of the registry for the *current* Thread.
    # Entries need to +respond_to?+ the method, otherwise a warning is logged.
    # Example: Call close on all entries of +:my_registry+
    #
    #   ThreadRegistry.call_all(my_registry: :close)
    #
    # @param opts (Hash) the calls to execute in form of {registry: :method} (both Symbols)
    def self.call_all(hash)
      ThreadRegistry.instance.call_all(hash)
    end

    # Yield each entry of the registry for the *current* Thread to a given block.
    # Example: Log all entries of +:metadata+
    #
    #    ThreadRegistry.yield_all(:metadata) { |entry| Tashi.log.info(entry.inspect) }
    #
    # @param key (Symbol) the registry to address
    # @param block (Block) the block to call, will get the entry as argument.
    def self.yield_all(key, &block)
      ThreadRegistry.instance.yield_all(key, &block)
    end

    # Add an entry to the registries for the *current* Thread.
    # Example: Add an instance to +:my_registry+
    #
    #    ThreadRegistry.register(my_registry: Foo.new)
    #
    # @param opts (Hash) the entries add in form of {registry: entry}
    def self.register(hash)
      ThreadRegistry.instance.register(hash)
    end

    # Remove all entries from the registry of the *current* Thread.
    def self.clear(key)
      ThreadRegistry.instance.clear(key)
    end

    # --- Instance methods

    # Call a method on all entries of the registry for the *current* Thread.
    # Entries need to +respond_to?+ the method, otherwise a warning is logged.
    # @param opts (Hash) the calls to execute in form of {registry: :method} (both Symbols)
    def call_all(opts)
      synchronized do
        opts.each do |key, method|
          self[key].compact.each { |entry| call_if_respond(entry, method) }
        end
      end
    end

    # Yield each entry of the registry for the *current* Thread to a given block.
    # @param key (Symbol) the registry to address
    # @param block (Block) the block to call, will get the entry as argument.
    def yield_all(key, &block)
      return unless block.present?

      synchronized do
        self[key].compact.each { |entry| block.call(entry) }
      end
    end

    # Add an entry to the registries for the *current* Thread.
    # @param opts (Hash) the entries add in form of {registry: entry}
    def register(opts)
      synchronized do
        opts.each { |key, value| self[key] << value }
      end
    end

    # Remove all entries from the registry of the *current* Thread.
    def clear(key)
      synchronized { self[key].clear }
    end

    private

    # This is a singleton! Use +#instace+ to acquire the instance
    def initialize
      @registry = {}
      @mutex = Mutex.new
    end

    def [](name)
      named_registry = @registry[name.to_sym] ||= {}
      named_registry[Thread.current] ||= []
    end

    def call_if_respond(obj, method)
      if obj.respond_to?(method)
        obj.public_send(method)
      else
        Tashi.log.warn("#{obj} does not respond to #{method}")
      end
    end

    def synchronized
      @mutex.synchronize { yield }
    end
  end
end
