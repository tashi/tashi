# frozen_string_literal: true

# Namespace
module Tashi
  ##
  # Module to provide Annotations for tests
  #
  # Author: Johannes Hoelken (johannes.hoelken@hawk.de)
  module Annotations
    ##
    # List of allowed annotations
    ANNOTATIONS = %i[_instruction _expectation _continue].freeze

    ##
    # Get the annotations for a given method, or all if no +method+ name is given.
    #
    # @param method: (Optional) The name of the method to get the annotations for
    #
    # @return the recorded annotations for a given method, or all annotations for all methods.
    def annotations(method: nil)
      return @annotations[method] unless method.nil?

      @annotations
    end

    private

    # Invoked as a callback whenever an instance method is added to the
    # receiver, thus we hook here to record the annotations we recorded so far.
    def method_added(method)
      @annotations = superclass.annotations.dup if !defined?(@annotations) && superclass.respond_to?(:annotations)
      @annotations = {} if !defined?(@annotations) || @annotations.nil?
      @annotations[method] = @last_annotation if defined?(@last_annotation) && !@last_annotation.nil?
      @last_annotation = nil
      super
    end

    def method_missing(method, *args)
      return super unless ANNOTATIONS.include?(method)

      @last_annotation ||= {}
      @last_annotation[method[1..-1].to_sym] = args.size == 1 ? args.first : args
    end

    # When using method_missing, define respond_to_missing?
    def respond_to_missing?(method, include_private = false)
      ANNOTATIONS.include?(method) || super
    end
  end
end

##
# Monkey Patch module to allow annotations
class Module
  private

  def annotate!
    extend Tashi::Annotations
  end
end
