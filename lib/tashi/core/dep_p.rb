# frozen_string_literal: true

##
# The Dependency Provider (+DepP+) is an Inversion of Control (+IoC+) Container
# to allow easy dependency injection (+DI+) in order to comply to the Dependency
# Inversion Principle (+DIP+) from "Clean Architecture".
#
# Any dependency can be registered as a "service", whereby a service can be anything
# from an instance, a class, a constant or even an arbitrary method reference (e.g. a Factory).
# Services are identified by Name (+Symbol+) and defined by a constructor block (+Lambda+).
#
# The constructor blocks are executed when the service is first requested (lazy loading) and are
# executed only once. The returned result is cached, thus if you have registered an instance it
# will effectively become a singleton.
#
# Services are requested by calling their field (created von the given name).
#
# All DepP methods are designed to be thread safe.
#
# Author johannes.hoelken@hawk.de
# Inspired by the KERNAL IoC from wolfgang.robben@scisys.com
#
# === Usage
# Services are registered by a symbol (as name) and a constructor block. The block can
# have the current DepP scope as (optional) parameter:
#
#   DepP.global.register(:answer) do |depp|
#     depp.log.debug("Answer computed")
#     return 42
#   end
#   DepP.global.register(:log) { Logger.new }
#
# Since the services are lazy initialized, we can even register the services in any order,
# as long as they are not requested outside of constructing blocks. To use the services we can
# call them by their field, e.g.
#
#   DepP.global.answer
#   => "[DEBUG] Answer computed"
#   => 42
#   DepP.global.answer
#   => 42
#
# Also recognize that there is no second log message from the constructor.
#
class DepP
  # Global synchronisation lock
  GLOBAL_LOCK = Monitor.new

  # Global instance reference
  @global_instance = nil

  ##
  # Returns the global DepP if you require it.
  # This is an optional wrapper to avoid every app needs to maintain a global DepP.
  # This instance is created on first request and never disposed.
  def self.global
    return @global_instance unless @global_instance.nil?

    GLOBAL_LOCK.synchronize do
      return @global_instance unless @global_instance.nil?

      return @global_instance = new
    end
  end

  ##
  # Creates a sub DepP forked from this one
  # @param new_scope - a new DepP to switch to. If nil, a new DepP is forked from the current global
  def self.switch_global_scope(new_scope = nil)
    new_scope = global.fork unless new_scope.is_a?(DepP)
    GLOBAL_LOCK.synchronize { return @global_instance = new_scope }
  end

  ##
  # Leaves the current scope and switches back to the parent scope of the current
  def self.pop_global_scope
    raise(IllegalStateError, 'Cannot pop: Parent DepP is not defined') unless global&.parent.is_a?(DepP)

    switch_global_scope(global&.parent)
  end

  ##
  # Normally, the instance method is called to query a service constructor.
  # If the class method is called, it means that the super DepP is pointing to the class which means that we reached
  # the end of the DepP chain. In this case, we don't have the service registered.
  # @param name - the service to request
  # @raise IllegalStateError - since the service is not registered
  def self.service_constructor(name)
    raise(IllegalStateError, "A service or method with the name '#{name}' is not registered in the DepP chain")
  end

  # ----- Instance methods ------------------------

  ##
  # Constructor.
  # DepPs can be chained if required. If so all requests not known to the current DepP are delegated to the parent.
  # @param parent - a super DepP to look for services if they cannot be resolved in this one.
  def initialize(parent = nil)
    @services = {}
    @instances = {}
    @instance_lock = Monitor.new
    @service_resolver = DepP # we look in the class if there is no super DepP
    return if parent.nil?
    raise(IllegalArgumentError, 'Parent should be a DepP') unless parent.is_a?(DepP)

    @service_resolver = parent
  end

  ##
  # Executes a given block in an instance-synchronization context
  def synchronized
    @instance_lock.synchronize { yield }
  end

  ##
  # Accessor to a parent DepP
  # @return parent or nil if there is none
  def parent
    @service_resolver.is_a?(DepP) ? @service_resolver : nil
  end

  ##
  # Creates forked DepP
  # The new DepP will have the current scope as parent
  def fork
    DepP.new(self)
  end

  ##
  # Register a new service
  # @param name - name under which the service shall be exposed.
  #    - Has to be a Symbol
  #    - must be unique
  # @param duplication - Controls what happens when service with the same name already exists:
  #    - :raise = raise exception,
  #    - :keep = keep the existing instance,
  #    - :replace = replace the existing instance
  # @param block - Service constructor. It is only called on demand (Lazy Initialisation)!
  #    The block can be a call to the service constructor, a database query or a simple constant.
  #    The block receives the current DepP as a parameter. So you can query for dependencies in the block.
  #
  def register(name, duplication = :raise, &block)
    raise(IllegalArgumentError, "Service name '#{name}' must be a Symbol") if name.nil? || !name.is_a?(Symbol)
    raise(IllegalArgumentError, "Service constructor for '#{name}' cannot be nil") if block.nil?

    synchronized do
      return if duplication?(name, duplication) && duplication == :keep

      unregister_unsynced(name)
      register_new_unsynced(name, block)
    end
  end

  ##
  # Registers a new service in this DepP if it does not exist yet.
  # @see register
  def register_or_keep(name, &block)
    register(name, :keep, &block)
  end

  ##
  # Re-registers a service in this DepP (overrides existing services, deletes cached instance).
  # If the service was not registered before, it's not a problem.
  # @see register
  def reregister(name, &block)
    register(name, :replace, &block)
  end

  ##
  # Removes a service from the current scope (not from super, if any!).
  # @param name - the name of the service to remove
  def unregister(name)
    synchronized do
      return if @services[name].nil?

      unregister_unsynced(name)
    end
  end

  ##
  # Deletes all instances of all service that where already created and cached.
  # **WARNING** This will break the singleton approach for instances. Since the cache is
  #     cleared the instance will be newly created *in the current scope*!
  # @param recursive - if true also all super DepPs will be cleared. Default: false
  def clear_cache!(recursive: false)
    synchronized { @instances = {} }
    @service_resolver.clear_cache!(recursive: true) if recursive && @service_resolver.is_a?(DepP)
  end

  ##
  # Queries for a service
  # Will look in the current scope first and - if not found - request from super scope.
  # @param name - the name of the service to request
  def [](name)
    # either we already have a service instance in our cache, or we call the service constructor
    # This allows for passing queries forward to a super scope.
    synchronized { @instances[name.to_sym] ||= service_constructor(name.to_sym)&.call(self) }
  end

  ##
  # Queries the service constructor of the service with the given name
  # @param name - service name (must be Symbol)
  def service_constructor(name)
    synchronized do
      return proc { @instances[name] } if @instances[name]

      @services[name] || @service_resolver&.service_constructor(name)
    end
  end

  ##
  # Missing-Method handler to intercept calls to not implemented methods
  # We hook in here and redirect to our service selector which returns the
  # requested service form this or any parent scope, if available
  # @param symbol - method name (Symbol)
  # @param _args - arguments passed
  #
  def method_missing(symbol, *_args)
    self[symbol]
  end

  # When using method_missing, define respond_to_missing?
  # @param symbol - method name (Symbol)
  # @param include_private - also lookup private and protected methods (default: false)
  def respond_to_missing?(symbol, include_private = false)
    self[symbol]
    true
  rescue StandardError
    super
  end

  private

  def duplication?(symbol, method = :raise)
    return false if @services[symbol].nil?
    raise(IllegalArgumentError, "the service '#{symbol}' is already registered") if method == :raise

    true
  end

  def unregister_unsynced(name)
    self.class.send(:undef_method, name) { self[name] } if respond_to?(name)
    @instances.delete(name)
    @services[name] = nil
  end

  # create a stub method in this scope to retrieve the service
  # remember the constructor block
  def register_new_unsynced(name, block)
    @services[name] = block
    self.class.send(:define_method, name) { self[name] }
  end
end
