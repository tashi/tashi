# frozen_string_literal: true

module Tashi
  ##
  # Exporter to generate +JSON+ representation of test procedures.
  #
  # Author johannes.hoelken@hawk.de
  #
  class JsonExporter
    require 'json'

    attr_reader :proc, :json

    ##
    # Class method shortcut to export a +Tashi::Procedure+ to JSON
    # If the target is left blank the method will return the JSON content as string
    # @param procedure the procedure to export
    # @param target file path of the export target
    def self.export(procedure, target = nil)
      return Tashi::JsonExporter.new(procedure).to_string if target.blank?

      Tashi::JsonExporter.new(procedure).to_file(target)
    end

    ##
    # Constructor
    # @param procedure the procedure to export
    def initialize(procedure)
      @proc = procedure
      @json = {}
    end

    ##
    # Export the results to an XML string
    def to_string
      description
      execution_summary
      steps
      JSON.pretty_generate(json)
    end

    def to_file(target)
      File.open(target, 'w') { |file| file << to_string }
    end

    private

    # ABC Size is too high, but as we assign data to a hash that's fine here.
    # rubocop:disable Metrics/AbcSize
    def description
      json[:name] = proc.name
      json[:description] = proc.description
      json[:pass_criteria] = proc.pass_criteria
      json[:requirements] = proc.requirements if proc.requirements.present?
      json[:sprs] = proc.sprs if proc.sprs.present?
    end
    # rubocop:enable Metrics/AbcSize

    def execution_summary
      execution_metadata
      summary(:observation)
      summary(:failure)
    end

    # ABC Size is too high, but as we assign data to a hash that's fine here.
    # rubocop:disable Metrics/AbcSize
    def execution_metadata
      json[:execution] = {
        result: proc.result,
        time: {
          start: proc.start_time&.iso8601,
          end: proc.end_time&.iso8601,
          duration: duration(proc.start_time, proc.end_time)
        },
        summary: proc.summary
      }
    end
    # rubocop:enable Metrics/AbcSize

    def summary(type)
      data = tests_by_status(type)
      return if !defined?(data) || data.blank?

      json[:execution]["#{type}s".to_sym] =
        data.each_with_object({}) do |entry, result|
          result[entry.name] = entry.public_send("#{type}s".to_sym)
        end
    end

    def tests_by_status(status)
      return proc.tests.reject { |e| e.observations.blank? } if status == :observation
      return proc.tests.reject { |e| e.failures.blank? } if status == :failure

      nil
    end

    def steps
      json[:steps] = []
      proc.tests.each(&method(:step_details))
    end

    def step_details(step)
      details = {
        name: step.name,
        result: step.result,
        duration: duration(step.start, step.stop),
        instruction: step.instruction,
        expectation: step.expectation,
        summary: step.summary
      }
      details[:log] = step.log if step.log.present?
      json[:steps] << details
    end

    def duration(start, stop)
      duration = ''
      return duration if start.blank? || stop.blank?

      ActiveSupport::Duration.build((stop - start).abs.to_i).parts.each do |k, v|
        duration += "#{v} #{k} "
      end
      duration.strip
    end
  end
end
