# frozen_string_literal: true

# Namespace
module Tashi
  ##
  # Data Item that holds information on the system under test.
  # It is filled from a simple yaml file
  #
  # Author johannes.hoelken@hawk.de
  class Environment
    require 'yaml_extend'

    attr_reader :config

    ##
    # Constructor
    #
    # @param input can be a name of an environment file or a path
    #
    def initialize(input)
      raise IllegalArgumentError, 'Environment is required' if input.nil?

      load_yaml(input)
      load_hosts
      register_with_depp
    end

    ##
    # The name of the test environment
    def name
      config[:name]
    end

    private

    def load_hosts
      config[:hosts]&.each(&method(:load_host))
    end

    # rubocop:disable Metrics/AbcSize
    def load_host(host)
      instance = DepP.global.host.new(name: host[:name], ip: host[:ip], fqdn: host[:fqdn])
      instance.type = host[:type] unless host[:type].nil?
      instance.properties = host[:properties] if host[:properties].is_a?(Hash)
      instance.ssh_properties = host[:ssh_properties] if host[:ssh_properties].is_a?(Hash)
    end
    # rubocop:enable Metrics/AbcSize

    def load_yaml(input)
      @config = YAML.ext_load_file(find_file(input)).keys_as_symbols!(recursive: true)
    end

    def find_file(input)
      return input if File.exist?(input.to_s)

      src = DepP.global.environment_source
      raise "Environments directory '#{src}' not found" unless Dir.exist?(src)

      env = File.join(src, "#{input}.yml")
      raise "Environment '#{env}' not found" unless File.exist?(env)

      env
    end

    ##
    # There can always be only one environment.
    # We enforce that always the newest one is registered as global.
    def register_with_depp
      DepP.global.reregister(:environment) { self }
    end
  end
end
