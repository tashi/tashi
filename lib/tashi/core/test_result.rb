# frozen_string_literal: true

##
# Namespace
module Tashi
  ##
  # Data item to store the result of a test method.
  # Author: johannes.hoelken@hawk.de
  class TestResult
    attr_reader :name, :instruction, :expectation, :observations, :failures, :continue, :start, :stop
    attr_accessor :result, :log

    def initialize(name:, instruction:, expectation:, continue: false)
      @name = name.to_s
      @instruction = instruction
      @expectation = expectation
      @continue = continue

      @observations = []
      @failures = []
      @result = Tashi::Result.default(:test)
      @start = DateTime.now
    end

    def finalize
      @stop = DateTime.now
      self
    end

    ##
    # check if the corresponding test is failed
    def failed?
      result == Tashi::Result::FAILED
    end

    ##
    # Check if the corresponding test has observations
    def observations?
      result == Tashi::Result::OBSERVATIONS
    end

    ##
    # Check if the corresponding test is passed
    def passed?
      result == Tashi::Result::PASSED
    end

    def not_yet?
      result == Tashi::Result::NOT_EXECUTED
    end

    ##
    # Get a summary text for reporting.
    def summary
      return 'The step was not executed.' if not_yet?

      summary = ''
      @observations.each { |o| summary += "OBS: #{o}\n" }
      summary += @failures.join("\n")
      summary = 'The expected observation was made.' if summary.blank?
      summary
    end
  end
end
