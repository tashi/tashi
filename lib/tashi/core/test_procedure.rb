# frozen_string_literal: true

# Namespace
module Tashi
  require_relative '../helper/assert'

  ##
  # Base class to create +TestProcedures+ in TASHI.
  # Derive from this class to create actual test procedures like this
  #
  #   class MyTest < Tashi::TestProcedure
  #    def initialize
  #      super(
  #        description: 'Test to demonstrate how easy it is to write tests.',
  #        pass_criteria: 'Procedure is passed if all tests methods are passed'
  #       )
  #     end
  #
  #     _instruction 'Perform basic addition'
  #     _expectation 'Two plus one equals three'
  #     def my_test_method
  #       Assert.equals(3, 2+1)
  #     end
  #   end
  #
  # Only annotated public methods will be recognized as test methods.
  #
  # Author johannes.hoelken@hawk.de
  #
  # rubocop:disable Metrics/ClassLength
  class TestProcedure
    annotate!

    include Assert

    attr_accessor :name, :description, :pass_criteria, :mandatory, :working_dir
    attr_reader :requirements, :sprs, :tests, :current_test, :start_time, :end_time

    def initialize(description: '', pass_criteria: '', name: self.class.name)
      @name = name
      @description = description
      @pass_criteria = pass_criteria

      @requirements = []
      @sprs = []
      @tests = []
    end

    ##
    # Execute all annotated tests within this procedure
    def execute
      setup
      self.class.annotations.each do |k, v|
        terminate? ? skip_test(k, v) : execute_test(k, v)
      end
      result
    ensure
      teardown
    end

    ##
    # Returns the result of the Procedure
    def result
      return Tashi::Result::NOT_EXECUTED if @tests.empty?
      return Tashi::Result::FAILED if @tests.any?(&:failed?)
      return Tashi::Result::OBSERVATIONS if @tests.any?(&:observations?)

      Tashi::Result::PASSED
    end

    ##
    # Check if the test is failed
    def failed?
      result == Tashi::Result::FAILED
    end

    ##
    # Check if test is passed (with observations)
    def passed?
      [Tashi::Result::OBSERVATIONS, Tashi::Result::PASSED].include?(result)
    end

    ##
    # Check if tests has been executed
    def executed?
      result != Tashi::Result::NOT_EXECUTED
    end

    ##
    # Return all observations and all failure messages
    def messages
      tests.each_with_object({}) do |test, result|
        result[test.name.to_sym] = { observations: test.observations, failures: test.failures }
      end
    end

    ##
    # Get a textual summary of the step results
    # rubocop:disable Metrics/AbcSize
    def summary
      sample = tests.reject { |t| %w[before_procedure after_procedure].include?(t.name) }
      p = sample.select { |t| t.passed? || t.observations? }.size
      f = sample.select(&:failed?).size
      ny = sample.select(&:not_yet?).size
      obs = sample.select(&:observations?).size
      "#{p} of #{sample.size} step(s) passed (#{obs} had observations), #{f} failed and #{ny} where not executed."
    end
    # rubocop:enable Metrics/AbcSize

    protected

    ##
    # Template method to do setup work.
    # Override this if your procedure needs any setup work to be done
    def before_procedure; end

    ##
    # Template method to do teardown work.
    # Override this if your procedure needs any cleanup work to be done
    def after_procedure; end

    ##
    # Call this method from within your test to add an Observation.
    # The Result will be set to +Result::OBSERVATIONS+, if the test is not failed already.
    def observation(message)
      @current_test.observations << message.to_s
      @current_test.result = Tashi::Result::OBSERVATIONS unless @current_test.failed?
    end

    ##
    # Call this method from within your test to fail the Test.
    # Note that this will not stop the execution, to prevent any following code from being executed
    # use +failure!+ instead
    def failure(message)
      Tashi.log.error("Step '#{@current_test.name}' Failed:\n#{message}")
      @current_test.failures << message.to_s
      @current_test.result = Tashi::Result::FAILED
    end

    ##
    # Call this method from within your test to fail the Test instantly.
    # Note that this will stop the execution and raise a +TestFailure+ Error.
    def failure!(message)
      raise TestFailure, message
    end

    private

    # rubocop:disable Metrics/MethodLength
    def setup
      @start_time = DateTime.now
      Tashi.log.info('##-- before_procedure --##')
      create_result('before_procedure',
                    instruction: 'setup everything needed for the procedure.',
                    expectation: 'This is possible.',
                    continue: false)
      before_procedure
    rescue TestFailure => e
      failure(e.message)
    rescue StandardError => e
      Tashi.exception(e, message: "Exception in #{name}#before_procedure")
      failure("[#{e.class.name}] Exception in before_procedure: #{e.message}\nSee log for further details.")
    ensure
      store_result
    end
    # rubocop:enable Metrics/MethodLength

    def teardown
      Tashi.log.info('##-- after_procedure --##')
      create_result('after_procedure',
                    instruction: 'cleanup after the procedure has run.',
                    expectation: 'This is possible.')
      after_procedure
    ensure
      @end_time = DateTime.now
      store_result
      close_all
    end

    def close_all
      DepP.global.closeable_helper.each do |helper|
        next unless helper.respond_to?(:close_hook)

        helper.close_hook
      end
    end

    def skip_test(name, metadata)
      create_result(name, metadata)
      store_result(Tashi::Result::NOT_EXECUTED)
    end

    def execute_test(name, metadata)
      Tashi.log.info("##-- Step '#{name}' --##")
      create_result(name, metadata)
      send(name)
    rescue TestFailure => e
      failure(e.message.to_s)
    rescue StandardError => e
      failure("[#{e.class.name}]: #{e.message}")
      Tashi.exception(e, message: 'Test Failed from unexpected exception.')
    ensure
      store_result
    end

    def create_result(name, metadata)
      Tashi.logger.reset
      @current_test = Tashi::TestResult.new(
        name: name,
        instruction: metadata[:instruction],
        expectation: metadata[:expectation],
        continue: metadata[:continue]
      )
    end

    def store_result(result = nil)
      @current_test.log = Tashi.logger.replay
      @current_test.result = result if Tashi::Result::ALL.include?(result)
      @tests << @current_test.finalize
    end

    def terminate?
      @tests.any? { |t| t.failed? && !t.continue }
    end
  end
  # rubocop:enable Metrics/ClassLength
end
