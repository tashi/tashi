# frozen_string_literal: true

# Namespace
module Tashi
  ##
  # Enum like class to provide test results
  # Author johannes.hoelken@hawk.de
  class Result
    # Test has not been executed (yet)
    NOT_EXECUTED = 'Not Executed'

    # Test is passed (Yey!)
    PASSED = 'Passed'

    # Test is failed (uh-oh!)
    FAILED = 'Failed'

    # Test is passed, but (unwanted) observations where made (partly yey!)
    OBSERVATIONS = 'Passed with observations'

    # List of all results
    ALL = [NOT_EXECUTED, PASSED, OBSERVATIONS, FAILED].freeze

    # Defines the default result
    # For +TestProcedures+ this is +NOT_EXECUTED+, for +Tests+ it's +PASSED+.
    def self.default(type = :proc)
      type == :proc ? NOT_EXECUTED : PASSED
    end
  end
end
