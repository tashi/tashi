# frozen_string_literal: true

# Namespace
module Tashi
  ##
  # Test executor will execute the individual test procedures
  #
  # Author johannes.hoelken@hawk.de
  #
  class TestExecutor
    attr_reader :group_name

    ##
    # Constructor
    # @param group_name the name of the group the executor works on behalf of
    def initialize(group_name)
      @group_name = group_name
    end

    ##
    # Runs a test procedure
    # @param test the procedure to execute
    def run_procedure(test)
      execute(test)
      check_result(test)
    ensure
      persist_result(test)
    end

    private

    def execute(test)
      Tashi.log.info("### Starting Procedure: #{test.name}.")
      working_dir = File.join(DepP.global.run_results, test.name.parameterize)
      FileUtils.mkdir_p working_dir.to_s
      test.working_dir = working_dir
      test.execute
    end

    # rubocop:disable Layout/LineLength
    def check_result(test)
      Tashi.log.info("Test '#{test.name}' Finished with result [#{test.result}]:\n\t#{test.summary}")
      raise TestFailure, "Mandatory test #{test.name} failed! Stopping Pool #{group_name}." if test.mandatory && test.failed?
    end
    # rubocop:enable Layout/LineLength

    def persist_result(test)
      save_to_file(test)
      save_to_db(test)
    end

    def save_to_file(test)
      if DepP.global.result_format == :json
        Tashi::JsonExporter.export(test, File.join(DepP.global.run_results, "#{test.name.parameterize}.json"))
      else
        Tashi::XmlExporter.export(test, File.join(DepP.global.run_results, "#{test.name.parameterize}.xml"))
      end
    end

    def save_to_db(test)
      return unless DepP.global.use_db?

      Tashi.log.debug("Writing #{test.name} result [#{test.result}] to database.")
      proc = Procedure.create_or_update!(test)
      update_steps(proc, test)
      persist_execution(proc, test)
    end

    def persist_execution(proc, test)
      DepP.global.run.procedure_results.create!(
          procedure: proc,
          run_group: group,
          result: test.result,
          start_time: test.start_time,
          end_time: DateTime.now,
          message: test.messages
      )
    end

    def update_steps(proc, test)
      test.tests.each { |step| ProcedureStep.create_or_update!(proc, step) }
    end

    def group
      return unless DepP.global.use_db?
      return @group if defined?(@group) && @group.present?

      @group = DepP.global.run&.groups&.find_by!(name: group_name)
    end
  end
end
