# frozen_string_literal: true

require 'singleton'
require 'logging'

# Namespace
module Tashi
  # Custom logger for the Tashi framework
  # It's a singleton as it handles i/o operations and global secret functionality
  #
  # Author johannes.hoelken@hawk.de
  #
  class Logger
    include Singleton

    LOCK = Mutex.new
    CONSOLE_FORMAT = Logging.layouts.pattern(pattern: '%T [%l] %d :: %m\n', date_pattern: '%Y-%m-%d %H:%M:%S.%L').freeze
    FILE_FORMAT = Logging.layouts.pattern(pattern: '%d %T [%l] :: %m\n', date_pattern: '%Y-%m-%d %H:%M:%S.%L').freeze

    # Set the logging level
    def level=(level = :info)
      Logging.logger.root.level = level
    end

    # get the current logging level
    def level
      Logging.logger.root.level
    end

    def debug?
      level.zero?
    end

    ##
    # Resets the current thread's logger (flushes everything from +#replay+)
    def reset
      LOCK.synchronize { @appender[thread_name]&.reopen }
    end

    # Gets a logger for the current thread and given name
    #
    # @param name optional custom name for the logger.
    # @param thread optional thread the logger is created on. Autodetected when not supplied.
    def log(name = nil, thread = nil)
      # Method needs to be synchronised in case several threads log in parallel.
      LOCK.synchronize do
        name = guess_caller(caller) if name.nil?
        acquire(name, thread) || register(name, thread)
      end
    end

    ##
    # Gets the output of the current thread's logger recorded so far
    def replay
      LOCK.synchronize { @appender[thread_name] ? @appender[thread_name].sio.string.dup : '' }
    end

    private

    # This is a singleton! Use +#instace+ to acquire the instance
    def initialize
      @loggers = {}
      @appender = {}

      @console_logger = Logging.appenders.stdout('stdout', layout: CONSOLE_FORMAT)
    end

    def thread_name(thread = nil)
      thread = Thread.current if thread.nil?
      thread[:name] || thread.to_s
    end

    def log_name(name, thread = nil)
      "#{name}[#{thread_name(thread)}]"
    end

    def acquire(name, thread = nil)
      l_name = log_name(name, thread)
      @loggers[thread_name(thread)]&.find { |log| log.name == l_name }
    end

    def register(name, thread = nil)
      log = new_logger(name, thread)
      t_name = thread_name(thread)
      @loggers[t_name] ||= []
      @loggers[t_name] << log
      log
    end

    def new_logger(name, thread = nil)
      logger = Logging.logger[log_name(name, thread)]
      logger.add_appenders(file_appender(thread))
      logger.add_appenders(@console_logger)
      logger
    end

    def file_appender(thread = nil)
      t_name = thread_name(thread)
      @appender[t_name] ||= Logging.appenders.string_io("sio_#{t_name}", layout: FILE_FORMAT)
    end

    def guess_caller(trace)
      return 'UNKNOWN' unless /^(.+?):(\d+)(?::in `(.*)')?/ =~ trace[2]

      caller = Regexp.last_match[1]
      caller = Regexp.last_match[1] if %r{/([^/]*).rb} =~ caller
      caller
    end
  end
end
