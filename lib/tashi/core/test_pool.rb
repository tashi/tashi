# frozen_string_literal: true

# Namespace
module Tashi
  ##
  # A test pool is a set of test procedures (@see TestProcedure). A pool
  # can have parent pools (pools it depend on) and child pools (pools
  # that depend on this one.) This allows to create a multi hierarchical dependency tree, e.g.
  #
  #    #        / - pool_3 - - - - - - \
  #    # pool_0 - pool_2.1 - pool_2.2 - - pool_4
  #    #        \ pool_1.1 - pool_1.2 /
  #
  # in this case the root pool (pool_0) is executed first and if it passed the
  # pools one, two and three will run in parallel. Once pools 2.1 and 3.1 are finished they will
  # allow their descendants to execute. Eventually once all five pools finished pool 4 will start.
  # Even multiple root pools with merging dependencies are supported.
  #
  # However, if you have build a loop in your dependency tree, e.g. with
  #
  #    # pool_a -> pool_b -> pool_a
  #
  # this is a valid pool configuration, but will raise an exception when you try to execute it, as
  # pool_a cannot be run twice.
  #
  # Pools also have a result, a name, a start and end time and print a nice summary via +#to_s+.
  #
  # Author johannes.hoelken@hawk.de
  #
  class TestPool
    attr_reader :result, :name, :start_time, :tests, :end_time, :parents, :children

    ##
    # Constructor
    # @param name the name of the pool
    # @param tests the tests to be executed within this pool
    # @param parents the parents of the pool (optional)
    def initialize(name:, tests:, parents: [], sleep_secs: 3)
      @result = Tashi::Result::NOT_EXECUTED

      @name = name
      @tests = tests
      @parents = parents
      @children = []
      @sleep_secs = sleep_secs
    end

    ##
    # Check if the current pool is executable
    def executable?
      result == Tashi::Result::NOT_EXECUTED && (root? || parents.all? { |p| p.result == Tashi::Result::PASSED })
    end

    ##
    # Check if is this a root pool
    def root?
      parents.empty?
    end

    def wait_for_execution
      until executable?
        raise TestFailure, 'Parent pool failed!' if parents.any? { |p| p.result == Tashi::Result::FAILED }

        sleep(@sleep_secs)
      end
    end

    ##
    # Add a parent
    # @param (TestPool) parent the parent to add. Must be a TestPool.
    def add_parent(parent)
      unless result == Tashi::Result::NOT_EXECUTED
        raise IllegalStateError, 'can only add parents while not yet executed'
      end
      raise IllegalArgumentError, 'Given parent is not a TestPool' unless parent.is_a?(TestPool)

      parents << parent
    end

    ##
    # Add a child
    # @param (TestPool) child the child to add. Must be a TestPool.
    def add_child(child)
      unless result == Tashi::Result::NOT_EXECUTED
        raise IllegalStateError, 'can only add children while not yet executed'
      end
      raise IllegalArgumentError, 'Given child is not a TestPool' unless child.is_a?(TestPool)

      children << child
    end

    ##
    # Execute all tests in this pool
    def execute
      raise IllegalStateError, 'Pool is not executable!' unless executable?

      started
      tests.each(&method(:run_procedure))
    rescue TestFailure => e
      Tashi.log.info(e.message)
      @result = Tashi::Result::FAILED
    ensure
      finished
    end

    ##
    # Test counter
    # @param state the state of the tests to count. Supported values
    #    - :passed wil count only passed tests
    #    - :failed wil count only failed tests
    #    - :not_exec wil count only tests not executed
    #    - :all wil count all tests (default)
    # @return the number tests within the pool
    def count(state = :all)
      case state
      when :passed
        tests.select(&:passed?).size
      when :failed
        tests.select(&:failed?).size
      when :not_exec
        tests.reject(&:executed?).size
      else
        tests.size
      end
    end

    ##
    # String representation of pool
    def to_s
      summary = "TestPool '#{name}' [#{@result}]:"
      summary += " #{count(:passed)} of #{count} passed,"
      summary += " #{count(:failed)} failed"
      summary += " and #{count(:not_exec)} not executed."
      summary
    end

    private

    def started
      Tashi.log.info("Starting pool: #{name}.")
      @start_time = DateTime.now
    end

    def finished
      @end_time = DateTime.now
      @result = Tashi::Result::PASSED unless result == Tashi::Result::FAILED
      Tashi.log.info("Pool finished. Summary:\n\t#{self}")
    end

    def run_procedure(test)
      executor.run_procedure(test)
    end

    def executor
      return @executor if defined?(@executor) && @executor.present?

      @executor = Tashi::TestExecutor.new(name)
    end
  end
end
