# frozen_string_literal: true

##
# Registration file.
# Register internal dependencies to be provided via +DepP+ here
#
# Author johannes.hoelken@hawk.de
module Tashi
  DepP.global.register_or_keep(:use_db?) { false }

  DepP.global.register_or_keep(:config_path) { 'config' }

  DepP.global.register_or_keep(:logger) do
    Tashi::Logger.instance
  end

  # Tashi close hook registry.
  # Register your closable helper via
  #   DepP.global.closeable_helper << HelperClass
  # and ensure your class implements self.close_hook
  DepP.global.register_or_keep(:closeable_helper) { [] }
end
