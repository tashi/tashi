# frozen_string_literal: true

##
# Registration file.
# Register default +DepP+ properties that should be set here
#
# Author johannes.hoelken@hawk.de
module Tashi
  DepP.global.register_or_keep(:test_source) do
    Tashi.log.info("Using default test source 'procedures/'.")
    File.join(Dir.pwd, 'procedures')
  end

  DepP.global.register_or_keep(:suite_source) do
    Tashi.log.info("Using default test source 'suites/'.")
    File.join(Dir.pwd, 'suites')
  end

  DepP.global.register_or_keep(:environment_source) do
    Tashi.log.info("Using default environment source 'environments/'.")
    File.join(Dir.pwd, 'environments')
  end

  DepP.global.register_or_keep(:config_source) do
    Tashi.log.info("Using default configuration source 'config/'.")
    File.join(Dir.pwd, 'config')
  end

  DepP.global.register_or_keep(:results_dir) do
    Tashi.log.info("Using default results source 'results/'.")
    File.join(Dir.pwd, 'results')
  end

  DepP.global.register_or_keep(:testfiles_source) do
    Tashi.log.info("Using default test files source 'testfiles/'.")
    File.join(Dir.pwd, 'testfiles')
  end

  DepP.global.register_or_keep(:result_format) do
    :xml
  end
end
