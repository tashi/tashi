# frozen_string_literal: true

require_relative '../helper/assert'
require_relative '../helper/host'
require_relative '../helper/name_generator'
require_relative '../helper/test_file'
require_relative '../helper/utils'

##
# Registration file.
# Register default helper classes and providers here.
#
# Author johannes.hoelken@hawk.de
module Tashi
  DepP.global.register(:host) { Host }
end
