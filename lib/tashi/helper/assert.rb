# frozen_string_literal: true

require 'fileutils'

##
# Main Assertion Module for +TASHI+.
# To provide your own assertions just extend the +Assert+ module in your gem or test project
#
# Author johannes.hoelken@hawk.de
module Assert
  ##
  # Sub module to provide equality assertions
  module Equal
    ##
    # Asserts that two objects are identical
    # @param expected (Object) the expected object
    # @param actual (Object) the actual object to assert
    # @param msg (String) an optional failure reason message
    # @raise (TestFailure) if objects are not '==' equal
    def self.objects(expected, actual, msg = nil)
      unless actual.is_a?(expected.class)
        Assert.flunk("Expected #{actual.class} to be a #{expected.class}. #{msg}".chomp)
      end
      Assert.flunk("Expected #{actual.inspect} to equal #{expected.inspect}. #{msg}".chomp) unless actual == expected
    end

    ##
    # Asserts that two local files are identical
    # @param expected (String) the file_path of the expectation / baseline file
    # @param actual (String) the file_path of the actual to assert
    # @param msg (String) an optional failure reason message
    # @raise (TestFailure) if files are not identical
    def self.files(expected, actual, msg = nil)
      identical = FileUtils.identical?(actual, expected)
      Assert.flunk("Files do not match. #{msg}".chomp) unless identical
    end

    ##
    # Asserts that two Arrays have identical content, ignoring the sorting
    # @param expected (Array) the expected array
    # @param actual (Object) the actual array to assert
    # @param msg (String) an optional failure reason message
    # @raise (TestFailure) if arrays are not equal or the actual is not an (Array)
    # rubocop:disable Style/RedundantInterpolation
    def self.array(expected, actual, msg = nil)
      Assert.flunk("Expected #{actual.class} to be an Array. #{msg}".chomp) unless expected.is_a?(Array)

      a_sort = actual.sort { |a, b| "#{a}" <=> "#{b}" }
      e_sort = expected.sort { |a, b| "#{a}" <=> "#{b}" }
      Assert.flunk("Expected #{a_sort.inspect} to equal #{e_sort.inspect}. #{msg}".chomp) unless a_sort == e_sort
    end
    # rubocop:enable Style/RedundantInterpolation
  end

  ##
  # Asserts that two objects are equal
  # Depending on the expectation different asserts will be used from the +Assert::Equal+ module.
  #
  # @param expected (Object) the expected object
  # @param actual (Object) the actual object to assert
  # @param msg (String) an optional failure reason message
  # @raise (TestFailure) if objects are not equal
  def self.equal(expected, actual, msg = '')
    if expected.is_a?(Array)
      Equal.array(expected, actual, msg)
    elsif File.exist?(expected.to_s) && !File.directory?(expected.to_s)
      Equal.files(expected.to_s, actual.to_s, msg)
    else
      Equal.objects(expected, actual, msg)
    end
  end

  ##
  # Assert that a string contains a substring N times.
  # @param substring (String) the expected substring
  # @param string (String) the actual string to check
  # @param occurrences (Integer) an optional count of expected occurrences
  # @raise (TestFailure) if string does not contain substring (exactly as often as expected or at all)
  # rubocop:disable Layout/LineLength
  def self.substring(string, substring, occurrences: nil)
    if occurrences.nil?
      Assert.flunk("'#{string}' doesn't contain expected substring '#{substring}'".chomp) unless string.include?(substring)
      return
    end

    count = string.scan(/(?=#{substring})/).count
    Assert.flunk("Expected '#{string}' to contain '#{substring}' #{occurrences.to_i} times, but found #{count}".chomp) unless count == occurrences.to_i
  end

  # rubocop:enable Layout/LineLength

  ##
  # Fails if the file with the given path does not include the expectation (String)
  # Multiline expectations are currently not supported!
  # @param expectation (String) the string that is expected to be present in the file
  # @param file_path (String) the file to assert
  # @param msg (String) an optional failure reason message
  # @raise (TestFailure) if file does not contain the expected string
  def self.file_contains(file_path, expectation, msg = nil)
    return if file_includes?(file_path, expectation)

    Assert.flunk("File '#{file_path}' does not include '#{expectation}'. #{msg}".chomp)
  end

  ##
  # Fails if the file with the given path DOES include the expectation (String)
  # Multiline expectations are not supported!
  # @param expectation (String) the string that is NOT expected within the file
  # @param file_path (String) the file to assert
  # @param msg (String) an optional failure reason message
  # @raise (TestFailure) if file does contain the unexpected string
  def self.file_not_contains(file_path, expectation, msg = nil)
    return unless file_includes?(file_path, expectation)

    Assert.flunk("File '#{file_path}' DOES include '#{expectation}'. #{msg}".chomp)
  end

  ##
  # Assert if an object is +truthy+. In ruby that is: Not +nil+ and not +false+
  # @param object (Object) the object to assert
  # @param msg (String) an optional failure reason message
  # @raise (TestFailure) if object is nil or false
  def self.truthy(object, msg = nil)
    Assert.flunk("Expected #{object.inspect} to be Truthy. #{msg}".chomp) unless object
  end

  ##
  # Assert if an object is +falsy+. In ruby that is either +nil+ or +false+.
  # @param object (Object) the object to assert
  # @param msg (String) an optional failure reason message
  # @raise (TestFailure) if object is not nil or false
  def self.falsy(object, msg = nil)
    Assert.flunk("Expected #{object.inspect} to be Falsy. #{msg}".chomp) if object
  end

  ##
  # Assert if an object is +blank+, that is +nil+ (or +#empty?+ is true)
  # @param object (Object) the object to assert
  # @param msg (String) an optional failure reason message
  # @raise (TestFailure) if object is not nil or empty
  def self.blank(object, msg = nil)
    Assert.flunk("Expected #{object.inspect} to be blank. #{msg}".chomp) if object.present?
  end

  ##
  # Assert if an object is +present+, that is not +nil+ (and not +#empty?+)
  # @param object (Object) the object to assert
  # @param msg (String) an optional failure reason message
  # @raise (TestFailure) if object is nil or empty
  def self.present(object, msg = nil)
    Assert.flunk("Expected [#{object.inspect}] to be non-nil and non-empty. #{msg}".chomp) if object.blank?
  end

  ##
  # Fails unless expectation and actual have a relative error less than epsilon. That is
  #    |expected - actual| < epsilon
  # Used to compare float values to be "equal enough".
  # @param expected (Float) the expected object
  # @param actual (Float) the actual object to assert
  # @param epsilon (Float) an optional allowed deviation (default: 0.0001)
  # @param msg (String) an optional failure reason message
  # @raise (TestFailure) if objects are not equal
  def self.close_enough(expected, actual, msg: nil, epsilon: 0.0001)
    error = (expected - actual).abs
    Assert.flunk("Error is to big: |#{expected} - #{actual}| = #{error} > #{epsilon}\n#{msg}".chomp) if error > epsilon
  end

  ##
  # A call to this method will fail the TestProcedure instantly.
  # @raise (TestFailure)
  def self.flunk(message)
    raise TestFailure, message
  end

  ##
  # Helper to check if a file includes an expectation.
  # Rather use +Assert.file_contains+ or +Assert.file_not_contains+ for assertions.
  # @param file_path (String) the path of the file to check
  # @param expectation (String) the content to check for
  # @return true/false
  def self.file_includes?(file_path, expectation)
    if expectation.include?("\n")
      raise IllegalArgumentError, 'Expectation contains line breaks, which is currently not supported!'
    end

    IO.foreach(file_path)&.lazy&.any? { |line| line.include?(expectation) }
  end
end
