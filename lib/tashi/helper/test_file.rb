# frozen_string_literal: true

require 'digest/md5'

# Helper class to provide typical support when dealing with test files.
#
# The class can fetch files from the global +testfile_source+, generate files with random content
# and a given minimal size or just compute the md5 sum of a file.
#
# Author johannes.hoelken@hawk.de
#
class TestFile
  NUMBERS = %w[0 1 2 3 4 5 6 7 8 9].freeze
  ALPHABET_UPPER = %w[A B C D E F G H I J K L M N O P Q R S T U V W X Y Z].freeze
  ALPHABET_LOWER = %w[a b c d e f g h i j k l m n o p q r s t u v w x y z].freeze
  SPECIALS = ['!', '?', '=', '_', '-', ':', ';', ',', '.', ' '].freeze

  CHARSET = (NUMBERS + ALPHABET_LOWER + ALPHABET_UPPER + SPECIALS).freeze

  # Generate a file with random string content in the working dir
  # @param working_dir (String) The dir to create the file in, e.g. the working_dir of the test procedure
  # @param extension (String) The file extension (default: txt)
  # @param min_size (Integer) The minimal size of the test file in bytes (default: 4567)
  # @param prefix (String) A prefix for the file name (default: 'RandomTestFile').
  # @return (String) the path of the generated file
  def self.generate(working_dir, min_size: 4567, prefix: 'RandomTestFile', extension: 'txt')
    TestFile.new(working_dir).generate(extension: extension, min_size: min_size, prefix: prefix)
  end

  # Generates a file from a template.
  #
  # The method will replace all occurrences of variables in the format +${key}+ with the given
  # replacement from the replacements hash. Assume the templates content is 'foo ${var} baz' and
  # the given replacement is "{var: 'bar'}" then the result would be 'foo bar baz'.
  #
  # The file name and extension of the generated file is derived from the templates file name.
  #
  # @param working_dir (String) The dir to create the file in, e.g. the working_dir of the test procedure
  # @param template (String) template file in the global testfiles source directory
  # @param replacements (Hash) The replacements map as {key => replacement}.
  # @return (String) the path of the generated file
  def self.from_template(working_dir, template, replacements)
    TestFile.new(working_dir).from_template(template, replacements)
  end

  # Fetch a file from the global testfiles source
  # @param (String) The 'path/to/testfile.txt' within the global testfiles source
  # @return (String) The completed file path
  def self.fetch(file)
    File.join(DepP.global.testfiles_source, file)
  end

  # Returns the md5sum of the file
  # @param (String) the file to generate MD5 for
  # @return (String) the md5 sum of the file
  def self.md5sum(file)
    Digest::MD5.new.file(file).hexdigest
  end

  # Constructor
  # @param working_dir (String) The dir to create the file in, e.g. the working_dir of the test procedure
  def initialize(working_dir)
    @working_dir = working_dir
  end

  # Generate a file with random string content in the working dir
  # @param extension (String) The file extension (default: txt)
  # @param min_size (Integer) The minimal size of the test file in bytes (default: 4567)
  # @param prefix (String) A prefix for the file name (default: 'RandomTestFile').
  # @return (String) the path of the generated file
  def generate(min_size: 4567, prefix: 'RandomTestFile', extension: 'txt')
    file = File.join(working_dir, "#{prefix}_#{timestamp}.#{extension}")
    Tashi.log.info("Generating #{File.basename(file)} with random content and minimum size: #{min_size}")
    File.open(file, 'w') do |f|
      f.puts("#{prefix} generated #{Time.now} by TASHI")
      f.puts(random(rand(50..70)).to_s) while f.size < min_size
    end
    file
  end

  # Generates a file from a template.
  #
  # The method will replace all occurrences of variables in the format +${key}+ with the given
  # replacement from the replacements hash. Assume the templates content is 'foo ${var} baz' and
  # the given replacement is "{var: 'bar'}" then the result would be 'foo bar baz'.
  #
  # The file name and extension of the generated file is derived from the templates file name.
  #
  # @param template (String) The 'path/to/template.xml' in the global testfiles source directory
  # @param replacements (Hash) The replacements map as {key => replacement}.
  # @return (String) the path of the generated file
  def from_template(template, replacements)
    template = TestFile.fetch(template)
    content = File.read(template)
    replacements.each { |k, v| content.gsub!("${#{k}}", v) }
    file_name = File.join(working_dir, "#{File.basename(template, '.*')}_#{timestamp}#{File.extname(template)}")
    Tashi.log.info("Generating #{File.basename(file_name)} from #{template}.")
    File.open(file_name, 'w') { |file| file.puts(content) }
    file_name
  end

  # Generate a file name save timestamp from the given time
  # @param time (Time) The time to generate timestamp for (Default: now)
  # @return (String)
  def timestamp(time = Time.now)
    time.strftime('%Y%m%d-%H%M%S')
  end

  private

  attr_reader :working_dir

  def random(length)
    chars = []
    length.times { chars << CHARSET.sample }
    chars.join
  end
end
