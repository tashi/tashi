# frozen_string_literal: true

require 'ipaddr'

##
# Data Item class to hold relevant information on a +Host+
# A host can be anything from a +:physical+ server, a +:virtual+ machine
# or a docker +:container+.
#
# Hosts are identified by their name and can be queried as globals from the
# +DepP+ once configured.
#
# Author johannes.hoelken@hawk.de
class Host
  ##
  # Find a registered host by its name
  # @param name - the name of the host to find
  def self.find(name)
    DepP.global.register_or_keep(:known_hosts) { [] }
    DepP.global.known_hosts.find { |h| h.name == name }
  end

  ##
  # Check if a Host with that name exists already
  # @param name - the name of the host to check
  def self.exists?(name)
    !find(name).nil?
  end

  attr_reader :name, :ip, :fqdn, :type
  attr_writer :type

  # All Net::SSH::VALID_OPTIONS properties are supported.
  # Unsupported SSH properties are ignored.
  attr_accessor :ssh_properties

  # Add generic properties to the host.
  # Can be looked up with Host.fetch('name')[prop_key]
  attr_accessor :properties

  ##
  # Constructor
  #
  # @param name - the name of the Host object, where it will be identified by (therefore must be unique)
  # @param ip - the IP Address of the host, e.g. '127.0.0.1'
  # @param fqdn - the Fully Qualified Domain Name, e.g. 'localhost'
  # @param type - the type to set. Should be one of :physical, :virtual, :container or :unknown (default)
  # At least one of +ip+ or +fqdn+ must be set.
  #
  def initialize(name:, ip: nil, fqdn: nil, type: :unknown)
    raise IllegalArgumentError, 'At least one of ip or fqdn must be set' if ip.nil? && fqdn.nil?
    raise IllegalArgumentError, "A host with name #{name} already exists!" if Host.exists?(name)

    @ip = IPAddr.new(ip) unless ip.nil?
    @fqdn = fqdn
    @name = name
    @type = type
    @properties = {}
    @ssh_properties = {}
    DepP.global.known_hosts << self
  end

  ##
  # @return the hosts address. will prefer fqdn over ip address (if set).
  def address
    fqdn || ip&.to_s
  end

  ##
  # @return the ssh user of the host ('root' if none is set).
  def user
    ssh_properties[:user] || 'root'
  end

  ##
  # Fetch a property
  # @param key - the identifier of the property
  def [](key)
    @properties[key]
  end

  ##
  # Set a property
  # @param key - the identifier of the property
  # @param value - the value to set
  def []=(key, value)
    @properties[key] = value
  end
end
