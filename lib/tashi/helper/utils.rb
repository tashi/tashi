# frozen_string_literal: true

##
# Module to provide basic helper methods that are useful for generic testing activities
#
# Author johannes.hoelken@hawk.de
module Utils
  ##
  # The stopwatch takes and calls a +&block+ and stops the time needed for its execution.
  # Specify the code to measure as +block+ param for the stopwatch:
  #
  #    elapsed, result = Utils.stopwatch(:ms) { 2**42 }
  #
  # Supported time units are
  #   - :sec seconds (default),
  #   - :cs centi seconds,
  #   - :ms milli seconds,
  #   - :us micro seconds.
  #
  # @param unit (Symbol) (optional) the unit to measure the time in.
  # @return (Array) [time_elapsed (rounded up to integer), block_result]
  # rubocop:disable Metrics/MethodLength
  def self.stopwatch(unit = :sec)
    Tashi.log.info('Starting stopwatch')
    start_time = Process.clock_gettime(Process::CLOCK_MONOTONIC)
    result = yield
    end_time = Process.clock_gettime(Process::CLOCK_MONOTONIC)
    elapsed = end_time - start_time
    Tashi.log.info("Stopwatch stopped. Recorded #{elapsed} seconds")
    case unit
    when :cs then [(elapsed * 100).ceil, result]
    when :ms then [(elapsed * 1_000).ceil, result]
    when :us then [(elapsed * 1_000_000).ceil, result]
    else [elapsed.ceil, result]
    end
  end
  # rubocop:enable Metrics/MethodLength

  # Wait for a specific result to occur.
  # Method takes a block and will repeatedly execute it until the
  # block result is +truthy+ (neither nil nor false) or the timeout occurs.
  #
  #    Utils.wait_for('4 to equal 5', timeout: 2) { 4 == 5 }
  #    Utils.wait_for('file to be created') { File.exist?('/path/to/file') }
  #
  # Only use wait_for if you expect the event to happen. Otherwise you can just sleep and check if the
  # event did not happen as expected afterwards.
  #
  # @param text (String) descriptive text: What are we waiting for?
  # @param timeout (Integer) Max allowed time to wait [in seconds] (default: 30)
  # @param interval (Float) Time to wait between tries [in (fractions of) seconds] (default 4.5)
  # @param fail_on_timeout (Boolean) Flag to determine if the test shall be failed on timeout (default: true)
  def self.wait_for(text = 'block to return true', timeout: 30, interval: 4.5, fail_on_timeout: true)
    Tashi.log.info("Waiting for #{text}.")
    start = Time.now
    result = false
    until result || Time.now - start > timeout
      result = yield
      sleep(interval) unless result
    end
    Tashi.log.warn("Timeout while waiting for #{text}.") unless result
    Assert.flunk("Timeout while waiting for #{text}.") if !result && fail_on_timeout
    result
  end
end
