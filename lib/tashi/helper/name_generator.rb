class NameGenerator

  DIGRAPHS = %w[a ac ad ar as at ax ba bi bo ce ci co ch de di e ed en es ex fa fo fu ga ge gi gu h ha he in im is it je
    ju jo ka ky la le le lo mi mo na ne ne ni no o oo ob oi ol on or or os ou pe pi po qt re ro rr sa se so sh ta te
    ti to tu ty ud um un us ut va ve ve za zi].freeze

  SYLLABLES = DIGRAPHS + %w[cla clu cra cre dre dro nob pen pha phi pho sha she sta stu kak tha the thi thy tri lly
    tty zel zen].freeze

  WEIGHTED_SYLLABLES = [DIGRAPHS, DIGRAPHS, SYLLABLES].freeze

  ##### class methods ######

  # Generate a random composed name
  # @param separator (String) the separating character
  # @param parts (Integer) the number of individual names
  # @return (String) a composed name, e.g. "Folo-Cedro"
  def self.composed_name(separator: '-', parts: 2)
    names = []
    parts.times { names << NameGenerator.generate_name }
    names.join(separator)
  end

  # Generate a random name
  # @param length (Integer) the number of individual syllables
  # @return (String) a name, e.g. "Cedro"
  def self.generate_name(length = nil)
    if length.present?
      NameGenerator.new.single_name(length)
    else
      NameGenerator.new.single_name
    end
  end

  ##### instance methods ######

  # Generate a random name
  # @param length (Integer) the number of individual syllables
  # @return (String) a name, e.g. "Cedro"
  def single_name(length = random_length)
    name = generate(length)
    until pronounceable?(name)
      name = generate(length)
    end
    name.capitalize
  end

  private

  def pronounceable?(string)
    string !~ /.*[aeiou]{3}.*/i
  end

  def generate(length)
    (0...length).collect { WEIGHTED_SYLLABLES.sample.sample }.join
  end

  def random_length
    [2, 2, 3, 3, 3, 4].sample
  end

end