# frozen_string_literal: true

##
# Active record model for test run
#
# Author: johannes.hoelken@hawk.de
#
class TestRun < ActiveRecord::Base
  has_many :procedure_results
  has_many :group_relations
  has_many :run_groups, through: :group_relations, source: :parent

  validates :environment, presence: true
  validates :specification, presence: true
  validates :start_time, presence: true

  alias results procedure_results

  def groups
    run_groups.distinct
  end

  ##
  # @return the results sub folder name, derived from the start time
  def results_dir_name
    start_time.strftime('%Y-%m-%d_%H-%M-%S')
  end
end
