# frozen_string_literal: true

##
# Active record model for test run group
# Stores the groups that have been executed within this run along with their dependency.
# This allows to draw nice dependency and timing graphs in a WebMMI.
#
# Author: johannes.hoelken@hawk.de
#
class RunGroup < ActiveRecord::Base
  # Self-Referencing Many to Many relation using a linking table.
  has_many :parent_relations, class_name: GroupRelation.name, foreign_key: :child_id
  has_many :parents, through: :parent_relations
  has_many :child_relations, class_name: GroupRelation.name, foreign_key: :parent_id
  has_many :children, through: :child_relations

  has_many :procedure_results

  # Name must be present and unique (within the run)
  validates :name, presence: true, uniqueness: true

  def root?
    parent_relations.blank?
  end
end
