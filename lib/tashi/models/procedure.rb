# frozen_string_literal: true

##
# Active record model for test procedure
#
# Author: johannes.hoelken@hawk.de
#
class Procedure < ActiveRecord::Base
  has_many :procedure_results
  has_many :procedure_steps

  validates :name, presence: true
  validates :description, presence: true

  alias results procedure_results
  alias steps procedure_steps

  ##
  # Creates or updates the persisted +Procedure+ from a given +Tashi::TestProcedure+ instance.
  # If the +Tashi::TestProcedure+ has not changed the +updated_at+ field will not change
  # TODO test_proc.sprs and test_proc.requirements
  #
  # @param test_proc an instance of Tashi::TestProedure
  # @return the Procedure model instance synched with the Tashi::TestProcedure
  def self.create_or_update!(test_proc)
    proc = Procedure.find_by(name: test_proc.name)
    proc = Procedure.new(name: test_proc.name) if proc.blank?
    proc.description = test_proc.description
    proc.pass_fail = test_proc.pass_criteria
    proc.save!
    proc
  end
end
