# frozen_string_literal: true

##
# Active record model for test run group
# Stores the groups that have been executed within this run along with their dependency.
# This allows to draw nice dependency and timing graphs in a WebMMI.
#
# Author: johannes.hoelken@hawk.de
#
class GroupRelation < ActiveRecord::Base
  belongs_to :test_run
  belongs_to :parent, class_name: 'RunGroup'
  belongs_to :child, class_name: 'RunGroup'
end
