# frozen_string_literal: true

##
# Active record model for test procedure step
#
# Author: johannes.hoelken@hawk.de
#
class ProcedureStep < ActiveRecord::Base
  belongs_to :procedure

  validates :name, presence: true
  validates :instruction, presence: true
  validates :expectation, presence: true

  ##
  # Creates or updates the persisted +ProcedureStep+ from a given +Tashi::TestResult+ instance.
  # If the +Tashi::ProcedureStep+ has not changed the +updated_at+ field will not change
  #
  # @param proc (Procedure) the active record model
  # @param test_result (Tashi::TestResult) the step metadata
  # @return the Procedure model instance synched with the Tashi::TestResult name, instruction and observation
  def self.create_or_update!(proc, test_result)
    step = ProcedureStep.find_by(procedure_id: proc.id, name: test_result.name)
    step = ProcedureStep.new(procedure_id: proc.id, name: test_result.name) if step.blank?
    step.instruction = test_result.instruction
    step.expectation = test_result.expectation
    proc.update(updated_at: Time.now) if step.changed?
    step.save!
    step
  end
end
