# frozen_string_literal: true

##
# Active record model for test run procedure result
#
# Author: johannes.hoelken@hawk.de
#
class ProcedureResult < ActiveRecord::Base
  # List of accepted results
  RESULTS = Tashi::Result::ALL

  belongs_to :test_run
  belongs_to :procedure
  belongs_to :run_group

  validates :test_run_id, presence: true
  validates :procedure_id, presence: true
  validates :result, presence: true, inclusion: { in: RESULTS, message: '%<value> is not a valid result!' }
  validates :start_time, presence: true
  validates :end_time, presence: true

  alias group run_group
end
