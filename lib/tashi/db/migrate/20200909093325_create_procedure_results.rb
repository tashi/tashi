# frozen_string_literal: true

##
# Migration for ProcedureResult model
class CreateProcedureResults < ActiveRecord::Migration[6.0]
  # migration to create table
  def change
    create_table :procedure_results do |t|
      t.integer :test_run_id, null: false
      t.integer :procedure_id, null: false
      t.integer :run_group_id, null: false
      t.string :result, null: false
      t.string :message

      t.timestamp :start_time, null: false
      t.timestamp :end_time, null: false
    end

    add_foreign_key :procedure_results, :test_runs
  end
end
