# frozen_string_literal: true

##
# Migration run_groups
class RunGroups < ActiveRecord::Migration[6.0]
  # migration method
  # rubocop:disable Metrics/MethodLength
  def change
    create_table :run_groups do |t|
      t.integer :test_run_id, null: false
      t.string :name, null: false

      t.timestamp :start_time, null: true
      t.timestamp :end_time, null: true
    end

    create_table :group_relations, id: false do |t|
      t.integer :parent_id, null: false, foreign_key: true
      t.integer :child_id, null: false, foreign_key: true
    end

    add_foreign_key :run_groups, :test_runs
  end
  # rubocop:enable Metrics/MethodLength
end
