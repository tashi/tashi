# frozen_string_literal: true

##
# Migration to create test_runs table
class CreateTestRuns < ActiveRecord::Migration[6.0]
  def change
    create_table :test_runs do |t|
      t.string :environment, null: false
      t.string :specification, null: false
      t.string :name
      t.timestamp :start_time, null: false
      t.timestamp :end_time
    end
  end
end
