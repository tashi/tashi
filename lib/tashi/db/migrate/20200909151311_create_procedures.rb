# frozen_string_literal: true

##
# Migration create_procedures
class CreateProcedures < ActiveRecord::Migration[6.0]
  # migration method
  def change
    create_table :procedures do |t|
      t.string :name, null: false
      t.string :description, null: false
      t.string :pass_fail

      t.timestamps
    end

    add_foreign_key :procedure_results, :procedures
  end
end
