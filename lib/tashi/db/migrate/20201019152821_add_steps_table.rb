##
# Migration add_steps_table
class AddStepsTable < ActiveRecord::Migration[6.0]
  # migration method
  def change
    create_table :procedure_steps do |t|
      t.integer :procedure_id, null: false
      t.string :name, null: false
      t.string :instruction, null: false
      t.string :expectation, null: false

      t.timestamps
    end

    add_foreign_key :procedure_steps, :procedures
  end
end
