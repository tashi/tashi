# frozen_string_literal: true

##
# Migration remove_duration_from_groups
class RemoveDurationFromGroups < ActiveRecord::Migration[6.0]
  # migration method
  def change
    remove_column :run_groups, :start_time
    remove_column :run_groups, :end_time
  end
end
