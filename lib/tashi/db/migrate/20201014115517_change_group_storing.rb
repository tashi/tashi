# frozen_string_literal: true

##
# Migration change_group_storing
# rubocop:disable Layout/LineLength
class ChangeGroupStoring < ActiveRecord::Migration[6.0]
  # migration method
  def up
    add_column :group_relations, :test_run_id, :integer
    execute('UPDATE group_relations AS gr SET test_run_id = (SELECT test_run_id FROM run_groups WHERE id = gr.parent_id);')
    remove_column :run_groups, :test_run_id
    change_column :group_relations, :child_id, :integer, null: true
  end

  # revert!
  def down
    add_column :run_groups, :test_run_id, :integer
    execute('UPDATE run_groups AS rg SET test_run_id = (SELECT test_run_id FROM group_relations WHERE parent_id == rg.id);')
    remove_column :group_relations, :test_run_id
  end
end
# rubocop:enable Layout/LineLength
