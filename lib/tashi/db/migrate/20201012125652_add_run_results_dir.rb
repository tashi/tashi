# frozen_string_literal: true

##
# Migration add_run_results_dir
class AddRunResultsDir < ActiveRecord::Migration[6.0]
  # migration method
  def change
    add_column :test_runs, :results_dir, :string
  end
end
