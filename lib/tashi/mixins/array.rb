# frozen_string_literal: true

##
# Mixin/Extension for Array class
# Author johannes.hoelken@hawk.de
class Array
  ##
  # Calls +keys_as_symbols!+ on all entries that support that method
  # @see Hash#keys_as_symbols!
  #
  # @return the altered original array
  def keys_as_symbols!(recursive: false)
    each do |entry|
      entry.keys_as_symbols!(recursive: recursive) if entry.respond_to?(:keys_as_symbols)
    end
    self
  end

  ##
  # Calls +keys_as_symbols+ on all entries that support that method
  # @see Hash#keys_as_symbols
  #
  # @return a copy of the original array
  def keys_as_symbols(recursive: false)
    each_with_object([]) do |entry, copy|
      copy <<
        if entry.respond_to?(:keys_as_symbols)
          entry.keys_as_symbols(recursive: recursive)
        else
          entry
        end
    end
  end
end
