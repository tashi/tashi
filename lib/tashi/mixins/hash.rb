# frozen_string_literal: true

##
# Mixin/Extension for Hash class
# Author johannes.hoelken@hawk.de
class Hash
  ##
  # Creates a new Hash where all keys are replaced with symbols (if +key.to_sym+ is possible).
  #
  # @param recursive if set to true also the value hashes will be symbolized. Default: false
  # @return a copy of the original hash with replaced keys
  def keys_as_symbols(recursive: false)
    each_with_object({}) do |(k, v), copy|
      copy[to_sym(k)] = recursive && v.respond_to?(:keys_as_symbols) ? v.keys_as_symbols(recursive: true) : v
    end
  end

  ##
  # Replaces all keys with symbols (if +key.to_sym+ is possible).
  # If you want to have a copy @see keys_as_symbols
  #
  # @param recursive if set to true also the value hashes will be symbolized. Default: false
  # @return current hash with replaced keys
  def keys_as_symbols!(recursive: false)
    keys.each do |k|
      v = delete(k)
      self[to_sym(k)] = recursive && v.respond_to?(:keys_as_symbols) ? v.keys_as_symbols!(recursive: true) : v
    end
    self
  end

  private

  def to_sym(key)
    key.to_sym
  rescue StandardError
    key
  end
end
