# frozen_string_literal: true

require 'yaml'
require 'erb'
require 'active_record'

require_relative 'tashi/mixins/mixins'

require_relative 'tashi/version'

require_relative 'tashi/core/core'
require_relative 'tashi/config/config'

require_relative 'tashi/models/models'

##
# Namespace
module Tashi
  # This must be a symbol for ActiveRecord#establish_connection
  DATABASE_ENV = :tashi

  ##
  # Shortcut to the global logger
  # @param name (optional) allows to set a custom name for the log output.
  # @return the logger for the current thread and name
  def self.log(name = nil)
    Tashi.logger.log(name)
  end

  ##
  # Access the registered logger
  def self.logger
    DepP.global.logger
  end

  ##
  # Utility method to log exceptions
  def self.exception(exception, message: "Exception in #{caller[0]}", simplify: true)
    log = "#{message}\n[#{exception.class.name}]: #{exception.message}"
    backtrace = exception.backtrace.present? ? exception.backtrace : []
    backtrace = backtrace.reject { |l| l =~ %r{\A[^:]*/gems/} } if simplify && !Tashi.logger.debug?
    Tashi.log.error("#{log}\n#{backtrace.join("\n")}")
  end

  def self.establish_db
    return if ActiveRecord::Base.connected?

    config_file = File.join(DepP.global.config_source, 'database.yml')
    raise "Database config not found at #{config_file}." unless File.exist?(config_file)

    conf_data = ERB.new(File.read(config_file)).result
    config = YAML.safe_load(conf_data, permitted_classes: [Symbol])
    ActiveRecord::Base.configurations = config
    ActiveRecord::Base.establish_connection config[Tashi::DATABASE_ENV]
    DepP.global.reregister(:use_db?) { true }
  end

  private

  # Use this annotation to note the test instructions for the step
  def _instruction; end

  # Use this annotation to note the expected observations for the test
  def _expectation; end

  # Use this annotation to note if the procedure shall continue even if this test fails
  def _continue; end
end
