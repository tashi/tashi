# frozen_string_literal: true

require 'minitest/autorun'

require_relative 'tashi_testing/in_memory_log'
require_relative 'tashi_testing/unit_test'
