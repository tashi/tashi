# frozen_string_literal: true

module Tashi
  # Base class for Tashi Unit Tests.
  #
  # This class will setup a temporary working directory which can be accessed via +@working_dir+.
  # The working directory will be cleaned up after the test automatically.
  #
  # It also switches the global +DepP+ scope and registers the working dir as results dir.
  # The previous scope is popped after each test.
  #
  # Author johannes.hoelken@hawk.de
  #
  class UnitTest < Minitest::Test
    def setup
      @working_dir = Dir.mktmpdir
      DepP.switch_global_scope
      DepP.global.reregister(:use_db?) { false }
      DepP.global.reregister(:results_dir) { @working_dir }
      DepP.global.reregister(:run_results) { @working_dir }
    end

    def teardown
      DepP.pop_global_scope
      FileUtils.remove_entry @working_dir
    end
  end
end
