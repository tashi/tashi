# frozen_string_literal: true

##
# Simple logger mock that stores all received messages in memory.
#
# To use this in your tests, just reregister the logger as
#       logger = InMemoryLog.new
#       DepP.global.reregister(:logger) { logger }
#
# Author johannes.hoelken@hawk.de
class InMemoryLog
  # Attributes can be read in testing to access the recorded messages as array
  # for each level
  attr_reader :info_messages, :debug_messages, :error_messages, :warn_messages, :fatal_messages

  # Set/get the current log_level
  attr_accessor :level

  ##
  # Constructor
  def initialize(level: :debug)
    @level = level
    @info_messages = []
    @debug_messages = []
    @error_messages = []
    @warn_messages = []
    @fatal_messages = []
  end

  def log(_name = nil)
    self
  end

  def debug(message)
    @debug_messages << message
  end

  def info(message)
    @info_messages << message
  end

  def error(message)
    @error_messages << message
  end

  def warn(message)
    @warn_messages << message
  end

  def fatal(message)
    @fatal_messages << message
  end

  def debug?
    level == :debug
  end

  ##
  # counts how often the given message was logged
  # @param msg (String) the message to find. Supports regex, must escape special characters, e.g. "[" with "\\["
  # @param level (Symbol) the log level to check, default: :info
  # @return (Integer) Count how often the given message occurs in the log.
  def times_logged(msg, level: :info)
    string =
      case level
      when :debug then @debug_messages.join('|||')
      when :error then @error_messages.join('|||')
      when :warn then @warn_messages.join('|||')
      when :fatal then @fatal_messages.join('|||')
      else @info_messages.join('|||')
      end
    string.scan(/(?=#{msg})/).count
  end

  ##
  # Checks if the given message can be found in the log at all.
  # @param msg (String) the message to find. Supports regex, must escape special characters, e.g. "[" with "\\["
  # @return (Boolean) true iff the message was logged at any level
  def ever_logged?(msg)
    %i[debug info warn error fatal].each do |level|
      return true if times_logged(msg, level: level).positive?
    end
    false
  end

  def never_logged?(msg)
    !ever_logged?(msg)
  end

  def replay
    ''
  end

  def reset; end
end
