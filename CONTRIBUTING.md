## Contribution Guide for TASHI

### Environment Preparation 
#### Ruby
**TASHI** is build on ruby. You need to have the ruby version defined in the [.ruby-version](.ruby-version) file 
installed within your system. 

We propose the use of [rbenv](https://github.com/rbenv/rbenv) 

#### SQLite3
One of the main purposes for **TASHI** is to store the test results in a long time database. 

In order to develop **TASHI** you therefore need to install [SQLite 3](https://www.sqlite.org/index.html) on your 
system, as a part of our tests run against an SQLite In Memory DB. On `Ubuntu` you can do that with

```bash
apt-get install sqlite3 libsqlite3-dev
```

#### Development Checkout

After checking out the repo, run `bin/setup` to install dependencies. 
Then, run `rake test` to run the tests. You can also run `bin/console` for an interactive prompt that 
will allow you to experiment.

To install this gem onto your local machine, run `bundle exec rake install`.

##### Test Coverage
As we are a test framework we take testing SERIOUS. 

To qualify for the use in critical domains (as space, health informatics, etc.) 
code coverage must be > 90% all the time. But also all other users will leverage from a well 
tested and documented framework. 

##### Code Style
We use [rubocop](https://rubocop.org/) with default/community settings and apply the 
[Ruby Community Styleguide](https://rubystyle.guide/). Our aim is that there are no `rubocop` findings in our code. 
However, in some cases an exception may be justifiable. 
Use `rubocop:disable` in the smallest possible scope in such cases, and justify in the Merge/Pull request.

##### API Documentation
We use [YARD](https://yardoc.org/) as API documentation tool..

The public api should be well documented, this means that
all Classes, Modules and public methods should have at least a short comment to explain their usage. 
Please also mark your authorship with your commit email-address, so that we know whom to blame. ;)

### Release 
To release a new version, update the version number in `version.rb`, and then run `bundle exec rake release`, 
which will create a git tag for the version, push git commits and tags, and push 
the `.gem` file to [rubygems.org](https://rubygems.org).

## Code of Conduct

Everyone interacting in the Tashi project's codebases, issue trackers, chat rooms and mailing lists is expected to follow the [code of conduct](CODE_OF_CONDUCT.md).
