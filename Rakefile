# frozen_string_literal: true

require 'bundler/gem_tasks'
require 'rake/testtask'

load 'lib/Rakefile'

namespace :test do
  desc 'Run the unit tests in test/unit'
  Rake::TestTask.new(:unit) do |t|
    t.libs << 'test'
    t.libs << 'lib'
    t.test_files = FileList['test/unit/**/*_test.rb']
    t.verbose = true
  end

  desc 'Run the tests that require a database in test/db'
  Rake::TestTask.new(:db) do |t|
    t.libs << 'test'
    t.libs << 'lib'
    t.test_files = FileList['test/db/**/*_test.rb']
    t.verbose = true
  end

  desc 'Run all tests'
  Rake::TestTask.new(:all) do |t|
    t.libs << 'test'
    t.libs << 'lib'
    t.test_files = FileList['test/**/*_test.rb']
  end
end

task test: ['test:all']

task default: :test
